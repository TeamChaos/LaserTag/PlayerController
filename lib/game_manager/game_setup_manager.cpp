#include "game_setup_manager.h"

#include "debug_handler.h"


#define LOOP_DELAY 1 //delay each iteration for 1 ms

using namespace game_management;
using namespace comm_interfaces;

GameSetupManager::GameSetupManager(WifiHandler *wh, GameComInterface *gci, IRHandler *ih, signal_output::LedController *lh, int playerID, bool useMobileData)
  : wh_(wh), gci_(gci), ih_(ih),lh_(lh),playerID_(playerID), useMobileData(useMobileData) {

  //init SETUP state machine with state DETACHED since there is no WIFI connection to base station yet
  currentSetupState = DETACHED;

  //countdown for launch not yet started
  timerStarted = false;

}

void GameSetupManager::loop() {
  switch (currentSetupState) {
    case DETACHED:
      lh_->setStatus(LED_STATUS_OFF,LED_STATUS_BLINK_SLOW);

      // try connecting to base station wifi network
      wh_->connect();
      //update SETUP state machine
      if(wh_->connected()){
        currentSetupState = CONNECTED;
        debug::DH.debug(TAG_SETUP,"Connected");
      }
      break;
    case CONNECTED:
      if(!wh_->connected()){
        debug::DH.warn(TAG_SETUP,"Lost Wifi connection again");
        currentSetupState = DETACHED; //Reset if connection lost
        break;
      }
      lh_->setStatus(LED_STATUS_BLINK_FAST,LED_STATUS_OFF);

      wh_->setupWebSocketServer();

      if(wh_->sendRegister(playerID_)){
        currentSetupState = REGISTERED;
        debug::DH.debug(TAG_SETUP,"Registered");
      }
      else {
        debug::DH.debug(TAG_SETUP,"Failed to register");
        delay(2000);
      }

      break;
    case REGISTERED:
      if(!wh_->connected()){
        debug::DH.warn(TAG_SETUP,"Lost Wifi connection again");
        currentSetupState = DETACHED; //Reset if connection lost
        break;
      }
      lh_->setStatus(LED_STATUS_BLINK_SLOW,LED_STATUS_OFF);
      // check if master has finished setup
      if (!gci_->setupFinished()) break;
      // if so request game setupState
      if (!wh_->requestGameSetup(playerID_,setup)) break;
      wh_->sendReady(playerID_);
      // update SETUP state machine
      currentSetupState = READY;
      break;

    case READY:
      if(timerStarted){
        lh_->setStatus(LED_STATUS_ON,LED_STATUS_OFF);
        lh_->setHealthLED(LED_RGB_PINK);
      }
      else{
        if(useMobileData && setup.launchTrigger == START_TIMER && !wh_->connected()){
          debug::DH.warn(TAG_SETUP,"Lost Wifi connection again");
          wh_->connect();
          if(!wh_->connected()){
            lh_->setStatus(LED_STATUS_OFF,LED_STATUS_BLINK_SLOW);
            lh_->setHealthLED(LED_RGB_BLUE);
            break;
          }
        }
        lh_->setStatus(LED_STATUS_BLINK_SLOW,LED_STATUS_OFF);
        lh_->setHealthLED(LED_RGB_OFF);
      }
      // game setup is now finished. Checking which signal type is used to launch the game and listening to it
      if(!gameStarted()) break;
      // go to EXIT state when game is launched
      currentSetupState = EXIT;
      break;

    case EXIT:
      break;

    default:
      break;
  }

  delay(LOOP_DELAY);

}

bool GameSetupManager::gameStarted() {
  //already define infrared and radio signals
  ir::IRSignal irSig;
  //check launch trigger type
  switch (setup.launchTrigger) {
    case START_TIMER:
      //if timer not started yet, wait for base stations radio signal to start timer
      if(!timerStarted) {
        //check if master sent signal to start timer
        if (!gci_->launchTimerStarted()) return false;
        debug::DH.info(TAG_SETUP,"Timer started");
        timerStarted = true;
        triggerStamp = millis();
      }
      //if timer already started, check if timeout exceeded
      else{
        int diff = millis() - triggerStamp;
        if(diff%1000==0){
          Serial.println( (diff/1000));
          delay(1);
        }
      }
      return timerStarted && ((millis() - triggerStamp) > setup.launchTime);


    case START_SPAWNPOINT:
      //listen to IR signals and check if spawnpoint reached
      if (!ih_->getSignal(irSig)) return false;
      //if signal received check if its the proper one
      return irSig.type == ir::SPAWNPOINT;
    default:
      return false;
  }
}

bool GameSetupManager::finished() {

  return currentSetupState == EXIT;

}

void GameSetupManager::reset() {

  currentSetupState = wh_->connected() ? CONNECTED : DETACHED;
  timerStarted = false;
}

gameSetup GameSetupManager::getGameSetup() {

  return setup;

}
