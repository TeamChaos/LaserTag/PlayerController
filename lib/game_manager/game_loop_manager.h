#ifndef GAMELOOPMANAGER_H
#define GAMELOOPMANAGER_H

#include "wifi_handler.h"
#include "ir_handler.h"
#include "radio_handler.h"

#include "feedback_handler.h"
#include "led_controller.h"

#include "entity_player.h"
#include "weapon.h"
#include "button_handler.h"

#include "game_setup.h"

#include "score_tracker.h"

using namespace comm_interfaces;

namespace game_management {

  class GameLoopManager {

  public:
    GameLoopManager(WifiHandler *wh, GameComInterface *gci, IRHandler *ir, ButtonHandler *bh, signal_output::LedController *lh,  signal_output::FeedbackHandler *fh,  entity::Weapon *weapon, int playerID);

    void loop();

    void reset();

    bool finished();

    /// Enables Weapon, sets speaker launch signal and leds
    void init(gameSetup &setup_);

    std::vector<game_analysis::scoreData>* getFinalResult();

    std::vector<int>* getHitRecords();

  private:
    //############# IR Signal Handling #############
    // retrieving ir signals and process them according to game setup
    void handleIRSignals();

    void IRHandleHit(ir::IRSignal sig);
    void IRHandleMed(ir::IRSignal sig);
    void IRHandleAmmo(ir::IRSignal sig);
    void IRHandleSpawn(ir::IRSignal sig);
    void IRHandleGrenade(ir::IRSignal sig);
    void IRHandleCapturePoint(ir::IRSignal sig);

    void processHit(int offenderID, int damage, int head, int body);

    //###############################################


    //#################################################
    void handleIncomingMessages();

    // Buton Handling
    void handleInputs();
    void buttonHandleSpawn();


    //########## General Game related methods
    //Respawn the player. weaponFirst: whether to delay health reset compared to weapon activation or reversed
    void handleSpawn(bool weaponFirst);
    void cycleLightMode();
    void sendLightMode();
    //Check if all devices are in the correct state. Try to fix any issues. Return false if unrecoverable error is found.
    bool selfCheck();
    // True as game is running
    bool running;

    // True if the wifi connection is lost during the game. Used to set LEDs accordingly
    bool isWifiBad;

    //communication interfaces
    WifiHandler *wh_;
    GameComInterface *gci_;
    IRHandler *ih_;
    ButtonHandler *bh_;

    //human device interfaces
    signal_output::LedController *lh_;
    signal_output::FeedbackHandler *fh_;


    //game setup
    gameSetup setup;

    //entities
    //player
    entity::Player *player;
    //players weapon
    entity::Weapon *weapon_;
    int playerID;
    int ticks;

    ///0 normal - 1 off - 2 Light
    int ledMode;

    std::vector<int> hit_records;
    bool recordHits =true;

  

    //instance of scoreTracker keeping track of kd stats
    game_analysis::ScoreTracker *scoreTracker;

  };
}
#endif
