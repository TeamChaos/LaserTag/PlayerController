#ifndef TESTMODEMANAGER_H
#define GAMESETUPMANAGER_H

#include "wifi_handler.h"
#include "radio_handler.h"
#include "ir_handler.h"
#include "game_setup.h"
#include "led_controller.h"
#include "feedback_handler.h"

#include "weapon.h"
#include "button_handler.h"

using namespace comm_interfaces;

namespace game_management {

  class TestModeManager {
    public:
        TestModeManager(WifiHandler *wh, IRHandler *ir, ButtonHandler *bh, signal_output::LedController *lh,  signal_output::FeedbackHandler *fh,  entity::Weapon *weapon);

        void loop();
        void setup();

    private:

        void selfCheck();
        void handleIRSignals();
        void handleInputs();
        void updateOutputs();

        int ticks =0;
        //communication interfaces
        WifiHandler *wh_;
        IRHandler *ih_;
        ButtonHandler *bh_;

        //human device interfaces
        signal_output::LedController *lh_;
        signal_output::FeedbackHandler *fh_; 

        entity::Weapon *weapon_;      

        //Test check list;
        /// Bitwise. Each unchecked detector is 1. If 0, everything has been checked
        int unCheckedDetectors; 
  };
}

#endif