#include "game_eval_manager.h"

using namespace game_management;

#define LOOP_DELAY 1 //delay each iteration for 1 ms


GameEvalManager::GameEvalManager(comm_interfaces::WifiHandler *wh,signal_output::LedController *lh,int playerID):wh_(wh),lh_(lh), playerID_(playerID) {
    currentEvalState = DETACHED;
}

void GameEvalManager::loop() {
  switch (currentEvalState) {
    case DETACHED:
      lh_->setStatus(LED_STATUS_BLINK_SLOW,LED_STATUS_BLINK_SLOW);
      lh_->setHealthLED(LED_RGB_BLUE);

      //update SETUP state machine
      if(wh_->connected()){
        currentEvalState = CONNECTED;
      }
      else{
        // try connecting to base station wifi network. For mobile data we should still be connected
        wh_->connect();
      }
      break;
    case CONNECTED:
      if(!wh_->connected()){
        currentEvalState = DETACHED;
        break;
      }
      lh_->setStatus(LED_STATUS_BLINK_FAST,LED_STATUS_OFF);
      if(wh_->submitResult(playerID_,data, hit_records)){
        currentEvalState = DONE;
        lh_->setStatus(LED_STATUS_OFF,LED_STATUS_OFF);
        lh_->setHealthLED(LED_RGB_BLUE);
      }
      break;
    default:
      break;
  }
  delay(LOOP_DELAY);

}

void GameEvalManager::reset(){
  currentEvalState = DETACHED;
}

bool GameEvalManager::finished() {
  return false; //currentEvalState == DONE; this would cause the manager to return to setup stage
}

void GameEvalManager::submitFinalResult(std::vector<game_analysis::scoreData>* data){
  this->data=data;
}

void GameEvalManager::submitHitRecords(std::vector<int>* hit_records){
  this->hit_records=hit_records;
}
