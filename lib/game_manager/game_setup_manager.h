#ifndef GAMESETUPMANAGER_H
#define GAMESETUPMANAGER_H

#include "wifi_handler.h"
#include "radio_handler.h"
#include "ir_handler.h"
#include "game_setup.h"
#include "led_controller.h"

using namespace comm_interfaces;

namespace game_management {

  class GameSetupManager {

  public:
    GameSetupManager(WifiHandler *wh, GameComInterface *gci, IRHandler *ih,  signal_output::LedController *lh,  int playerID, bool useMobileData);

    void loop();

    /// Tells if SETUP state machine terminated (EXIT state)
    bool finished();

    /// Returns game setup
    gameSetup getGameSetup();

    /**
    * Resets the SETUP state machine. State is set to CONNECTED, if device still connected
    * to base station WIFI, otherwise set to DETACHED
    */
    void reset();

  private:
    /// Checks launch trigger type and waits for corresponding signal
    bool gameStarted();

    //states of sub state machine setup
    enum setupState {DETACHED, CONNECTED,REGISTERED, READY, EXIT};
    setupState currentSetupState;

    //communication interfaces
    WifiHandler *wh_;
    GameComInterface *gci_;
    IRHandler *ih_;
    signal_output::LedController *lh_;

    // game relevant data
    gameSetup setup;

    //parameters for handling game launch
    // whether launch timer has been started in case of launch trigger type set to TIMER
    bool timerStarted;
    // timestamp when launch timer was triggered
    unsigned int triggerStamp;
    int playerID_;

    // whether we are connected to the master via mobile
    bool useMobileData;
  };
}
#endif
