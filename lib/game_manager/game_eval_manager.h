#ifndef GAMEEVALMANAGER_H
#define GAMEEVALMANAGER_H

#include "wifi_handler.h"
#include "led_controller.h"

namespace game_management {

  class GameEvalManager {

  public:
    GameEvalManager(comm_interfaces::WifiHandler *wh, signal_output::LedController *lh, int playerID);

    void loop();

    bool finished();

    void submitFinalResult(std::vector<game_analysis::scoreData>* data);

    void submitHitRecords(std::vector<int>* hit_records);

    void reset();

  private:

    //states of sub state machine setup
    enum setupState {DETACHED, CONNECTED, DONE};
    setupState currentEvalState;

    //communication interfaces
    comm_interfaces::WifiHandler *wh_;
    signal_output::LedController *lh_;
    int playerID_;


    std::vector<game_analysis::scoreData>* data;
    std::vector<int>* hit_records;
  };
}
#endif
