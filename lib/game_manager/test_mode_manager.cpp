#include "test_mode_manager.h"
#include "debug_handler.h"
#include "weapons.h"


using namespace game_management;


TestModeManager::TestModeManager(WifiHandler *wh, IRHandler *ih, ButtonHandler *bh, signal_output::LedController *lh,  signal_output::FeedbackHandler *fh,  entity::Weapon *weapon) : 
    wh_(wh),ih_(ih),bh_(bh),lh_(lh),fh_(fh),weapon_(weapon) {

}

void TestModeManager::setup(){
    unCheckedDetectors = 0b1111111111;
    updateOutputs();
    weapon_->configure(9,&data::weapons::defaultWeapon);
    weapon_->enable();
}

void TestModeManager::loop(){
    ticks++;
    if(ticks%1000==0){
        selfCheck();
    }
    handleIRSignals();
    if(ticks>3000){ //Give the user some time to release the test mode buttons
        handleInputs();
    }
    else
    {
        //Clear cached previous presses
        bh_->hasBtn1BeenPressed();
        bh_->hasBtn2BeenPressed();
        bh_->hasBtn3BeenPressed();
    }
    delay(1);
    
}

void TestModeManager::selfCheck(){
    uint8_t ir_status = ih_->checkStatus();
    if(ir_status!=0x00 ){
        debug::DH.warn(TAG_GAMELOOP,"Not detecting");
        if(ir_status==0x01){
        ih_->startDetecting();
        debug::DH.info(TAG_GAMELOOP,"Restarted detector");
        }
    }
}

void TestModeManager::handleIRSignals(){
    ir::IRSignal sig;
    if (!ih_->getSignal(sig)) return;
    debug::DH.debug(TAG_GAMELOOP,"IR signal received "+String(sig.type,HEX)+" "+String(sig.quantity));
    unCheckedDetectors = unCheckedDetectors & ~sig.rawReceivers;
    debug::DH.debug(TAG_TEST,"Unchecked receivers: "+String(unCheckedDetectors,BIN));
    bool activateMotors[6] = {sig.head & 0b1, sig.head & 0b10,  sig.head & 0b100, sig.head & 0b1000,  (sig.body & 0b10), (sig.body & 0b1)};
    fh_->triggerMotor(activateMotors, false);
    fh_->playSound(SOUND_ID_HURT);
    updateOutputs();
}

void TestModeManager::handleInputs(){
    if(bh_->hasBtn1BeenPressed()){
        bool activateMotors[6] = {true,true,true,true,true,true};
        fh_->triggerMotor(activateMotors,false);
    }
    else if(bh_->hasBtn2BeenPressed()){
        bool activateMotors[6] = {false,false,false,false,false,false};
        activateMotors[0]=true;
        fh_->triggerMotor(activateMotors,false);
        delay(3000);
        fh_->tick();
        activateMotors[0]=false;
        activateMotors[1]=true;
        fh_->triggerMotor(activateMotors,false);
        delay(3000);
        fh_->tick();
        activateMotors[1]=false;
        activateMotors[2]=true;
        fh_->triggerMotor(activateMotors,false);
        delay(3000);
        fh_->tick();
        activateMotors[2]=false;
        activateMotors[3]=true;
        fh_->triggerMotor(activateMotors,false);
        delay(3000);
        fh_->tick();
        activateMotors[3]=false;
        activateMotors[4]=true;
        fh_->triggerMotor(activateMotors,false);
        delay(3000);
        fh_->tick();
        activateMotors[4]=false;
        activateMotors[5]=true;
        fh_->triggerMotor(activateMotors,false);
        delay(3000);
    }
}

void TestModeManager::updateOutputs(){
    lh_->setHealthLED(LED_RGB_VIOLET);
    bool testComplete = !unCheckedDetectors;
    if(testComplete){
        lh_->setStatus(LED_STATUS_ON,LED_STATUS_OFF);
    }
    else{
        lh_->setStatus(LED_STATUS_OFF,LED_STATUS_ON);
    }

}

