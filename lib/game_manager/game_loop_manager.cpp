#include "game_loop_manager.h"
#include "weapons.h"
#include "debug_handler.h"
#include "score_tracker.h"


#ifndef LED_MODE_BRIGHT
#define LED_MODE_BRIGHT 1
#endif
#ifndef LED_MODE_OFF
#define LED_MODE_OFF 2
#endif

using namespace game_management;

GameLoopManager::GameLoopManager(WifiHandler *wh, GameComInterface *gci,
  IRHandler *ih,ButtonHandler *bh, signal_output::LedController *lh, signal_output::FeedbackHandler *fh , entity::Weapon *weapon, int playerID)
  : wh_(wh), gci_(gci), ih_(ih), bh_(bh), lh_(lh), fh_(fh), weapon_(weapon), playerID(playerID) {

}

void GameLoopManager::init(gameSetup &setup_) {
  //store game setup
  setup = setup_;
  int team;
  if(playerID>=setup.players.size()){
    debug::DH.error(TAG_GAMELOOP,"Cannot retrieve player info from setup");
    team=0;
  }
  else{
    team=setup.players.at(playerID).teamID;
    debug::DH.debug(TAG_GAMELOOP,"Team "+String(team));
  }
  //initialise player and weapon
  player = new entity::Player(playerID,team);
  player->setWeapon(setup.defaultWeapon);
  player->setRespawnDelay(setup.respawnDelay);

  //score tracker keeping track of kd stats
  scoreTracker = new game_analysis::ScoreTracker(playerID, setup);
  weapon_->configure(player->getID(),(player->getWeapon()));
  //todo enable Weapon, set speaker (start sound), set leds, set player characeristics etc
  weapon_->enable();

  //lh_->checkStatus();
  lh_->setStatus(LED_STATUS_ON,LED_STATUS_OFF);

  debug::DH.configure(player,weapon_);

  ir::IRSignal sig;
  ih_->getSignal(sig); //Get, clear and ignore potential cached IR signals
  bh_->clearButtonCache(); //Clear button handler

  hit_records = std::vector<int>(10) ;
  //game is now running
  running = true;
  ledMode = 0;
  ticks=0;

  debug::DH.debug(TAG_GAMELOOP,"Started game");
  fh_->playSound(SOUND_ID_GAME_START);

}

void GameLoopManager::loop() {
  ticks++;

  if(ticks%65536==0){
    selfCheck();
  }

  //time sensitive stuff
  player->tick();  
  weapon_->tick();


  // check ir signals
  handleIRSignals();
  
  // handle incoming Messages (either mobile or 433Mhz)
  handleIncomingMessages();

  handleInputs();


  //todo handle leds and sound
  if(player->healthChanged()){
    if(ledMode==LED_MODE_BRIGHT){
      lh_->setHealthLED(LED_RGB_WHITE);
    }
    else if(ledMode!=LED_MODE_OFF){
      lh_->setHealthLED(player->getHealth());
    }
  }

  //check termination conditions
    if(!running){
      weapon_->disable();
      fh_->playSound(SOUND_ID_GAMEEND);
    }
}

void GameLoopManager::reset(){
}


//############IR Signal Handling ###########

void GameLoopManager::handleIRSignals() {
  ir::IRSignal sig;
  if (!ih_->getSignal(sig)) return;
  debug::DH.debug(TAG_GAMELOOP,"IR signal received "+String(sig.type,HEX)+" "+String(sig.quantity));
  //signal received
  switch (sig.type) {
    case ir::HIT:
      IRHandleHit(sig);
      break;
    case ir::MEDKIT:
      IRHandleMed(sig);
      break;
    case ir::AMMUNITION:
      IRHandleAmmo(sig);
      break;
    case ir::SPAWNPOINT:
      IRHandleSpawn(sig);
      break;
    case ir::CAPTUREPOINT:
      IRHandleCapturePoint(sig);
      break;
    case ir::GRENADE:
      IRHandleGrenade(sig);
      break;
    default:
      break;
  }
}

void GameLoopManager::IRHandleCapturePoint(ir::IRSignal sig) {
  if(player->getCondition() == entity::ALIVE) {
    if(setup.mode == DOMINATION){
      int newPoints = scoreTracker->addGamePoints(setup.gameModePoints);
      gci_->transmitGamePoints(newPoints);
      fh_->playSound(SOUND_ID_GAMEPOINTS);
    }
    else if(setup.mode == CONQUEST){
      gci_->transmitConquestPointMesssage(sig.quantity);
    }
  }
}

void GameLoopManager::IRHandleGrenade(ir::IRSignal sig) {
  //player can't be hit if he is already dead or recently respawned
  if (player->getCondition() != entity::ALIVE) return;
  int offenderID = sig.quantity;
  if(recordHits){
    int value = sig.type << 16 | sig.rawReceivers;
    hit_records.push_back(value);
  }
  // offender is a team mate and friendly fire deactivated
  if(setup.teamBased && !scoreTracker->isEnemy(offenderID) && !setup.friendlyFire) return;
  
  if(setup.extendedUpdates)gci_->transmitDamageMessage(offenderID, 100000, DamageType::GRENADE );
  processHit(offenderID,100000, sig.head, sig.body);
}

void GameLoopManager::IRHandleHit(ir::IRSignal sig) {
  //player can't be hit if he is already dead or recently respawned
  if (player->getCondition() != entity::ALIVE) return;

  //player shot himself
  if (sig.offenderID == playerID) return;

  if(recordHits){
    int value = sig.type << 16 | sig.rawReceivers;
    hit_records.push_back(value);
  }

  // offender is a team mate and friendly fire deactivated
  if(setup.teamBased && !scoreTracker->isEnemy(sig.offenderID) && !setup.friendlyFire) return;

  if(setup.extendedUpdates)gci_->transmitDamageMessage(sig.offenderID, sig.quantity, sig.head ? DamageType::HEADSHOT : DamageType::SHOT );

  processHit(sig.offenderID, sig.quantity, sig.head, sig.body);
  
}

void GameLoopManager::processHit(int offenderID, int damage, int head, int body) {
  player->hit(damage,head,body);
  bool isDead = (player->getHealth() <= 0 );
  scoreTracker->updateHit(offenderID, isDead);
  if(isDead) {
    weapon_->disable();
    bool activateMotors[6] = {true,true,true,true,true,true};
    fh_->triggerMotor(activateMotors,true);
    fh_->playSound(SOUND_ID_DEATH);

    gci_->transmitScoreMessage(scoreTracker->getTransmitData());
  }
  else{
    bool activateMotors[6] = {head & 0b1, head & 0b10,  head & 0b100, head & 0b1000,  (body & 0b10), (body & 0b1)};
    fh_->triggerMotor(activateMotors, false);
    fh_->playSound(SOUND_ID_HURT);

  }
  //todo handle sound and leds
}

void GameLoopManager::IRHandleMed(ir::IRSignal sig) {
  if (player->getCondition() != entity::ALIVE) return;
  // todo: time dependent healing
  if(setup.extendedUpdates)gci_->transmitDamageMessage(-1, sig.quantity, DamageType::MEDKIT );

  player->cure(sig.quantity);
  fh_->playSound(SOUND_ID_HEAL);
}

void GameLoopManager::IRHandleAmmo(ir::IRSignal sig) {
  if (player->getCondition() != entity::ALIVE) return;
  // raise stock of ammunition
  weapon_->increaseAmmo(sig.quantity);
  fh_->playSound(SOUND_ID_REFILL);
  //handle sound and leds
}

void GameLoopManager::IRHandleSpawn(ir::IRSignal sig) {
  if(sig.quantity == -1 || sig.quantity == player->getTeam()){
    handleSpawn(true);
  }
  else{
    debug::DH.debug(TAG_GAMELOOP,"Received IR spawn for a different team");
  }
}

void GameLoopManager::buttonHandleSpawn() {
  if(setup.localRespawn){
    handleSpawn(false);
  }
  else{
    debug::DH.warn(TAG_GAMELOOP,"Asked to respawn locally, but disabled");
  }
}

void GameLoopManager::handleSpawn(bool weaponFirst) {
  if (player->getCondition() == entity::DEAD) {
    player->respawn(weaponFirst? setup.respawnOffset : 0);
    weapon_->configure(player->getID(), player->getWeapon());
    if(weaponFirst){
      weapon_->enable();
    }
    else{
      weapon_->enable(setup.respawnOffset);
    }
    fh_->playSound(SOUND_ID_RESPAWN);
    gci_->transmitRespawnMessage();
  }
  else if(player->getCondition() == entity::ALIVE){
      int magsLeft = weapon_->getMagazineCount();
      int magsInit = player->getWeapon()->initial_mags;
      if(magsLeft < magsInit){
        weapon_->setMagazineCount(magsInit);
        fh_->playSound(SOUND_ID_REFILL);
      }
  }
}

void GameLoopManager::handleIncomingMessages() {
  ScoreMessage scoreMsg;
  if (gci_->getReceivedScoreMessage(scoreMsg)) {
    if (scoreMsg.offenderID == playerID) {
      debug::DH.debug(TAG_GAMELOOP,"We disabled someone :D ");
      weapon_->confirmKill();
    }
    scoreTracker->updateAbsoluteHits(scoreMsg.offenderID, scoreMsg.kills, scoreMsg.victimID, scoreMsg.assists);
    scoreTracker->updateAbsoluteDeaths(scoreMsg.victimID, scoreMsg.deaths);  
  }
  int cpoint = gci_->cPointCaptured();
  if(cpoint!=-1){
    //Play sound if participated in point capture
    if(this->player->getHealth()>0&&this->player->getTeam()==cpoint){
      fh_->playSound(SOUND_ID_GAMEPOINTS);
    }
  }
  if (gci_->gameStopped()) {    
      //terminate game loop
      running = false;
      return;
  }
}

//###################   Input Handling #########################

void GameLoopManager::handleInputs(){
  if(bh_->hasBtn1BeenPressed()){
    buttonHandleSpawn();
  }
  if(bh_->hasBtn2BeenPressed()){
    cycleLightMode();
  }
}


//##############################################################
bool GameLoopManager::selfCheck(){
  uint8_t ir_status = ih_->checkStatus();
  if(ir_status!=0x00 ){
    debug::DH.warn(TAG_GAMELOOP,"Not detecting");
    if(ir_status==0x01){
      ih_->startDetecting();
      debug::DH.info(TAG_GAMELOOP,"Restarted detector");
    }
  }

  if(!wh_->checkWiFiInGame()){
      debug::DH.warn(TAG_GAMELOOP,"Wifi bad");
      if(!isWifiBad){
        isWifiBad=true;
        sendLightMode();
      }
  }
  else{
    if(isWifiBad){
      isWifiBad=false;
      sendLightMode();
    }
  }

  return true;
}

void GameLoopManager::sendLightMode(){
  if(isWifiBad){
      lh_->setStatus(LED_STATUS_ON,LED_STATUS_BLINK_SLOW);

  }
  else{
  switch(ledMode){
    case LED_MODE_BRIGHT:
      lh_->setHealthLED(LED_RGB_WHITE);
      lh_->setStatus(LED_STATUS_ON,LED_STATUS_OFF);
      break;
    case LED_MODE_OFF:
      lh_->setHealthLED(LED_RGB_OFF);
      lh_->setStatus(LED_STATUS_OFF,LED_STATUS_OFF);
      break;
    default:
      lh_->setHealthLED(player->getHealth());
      lh_->setStatus(LED_STATUS_ON,LED_STATUS_OFF);
      break;
  }
  }
}

void GameLoopManager::cycleLightMode(){
  debug::DH.info(TAG_GAMELOOP,"Cycle light mode");
  ledMode++;
  if(ledMode>2){
    ledMode=0;
  }
  sendLightMode();

}
//##############################################################

//##############################################################

bool GameLoopManager::finished() {
  return !running;
}

std::vector<game_analysis::scoreData>* GameLoopManager::getFinalResult(){
  return scoreTracker->getFinalResult();
}

std::vector<int>* GameLoopManager::getHitRecords(){
  return &hit_records;
}
