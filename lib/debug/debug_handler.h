#ifndef DEBUGHANDLER_H
#define DEBUGHANDLER_H

#include <Arduino.h>
#include <functional>

#include "weapon.h"
#include "entity_player.h"



#define LEVEL_ERROR 0
#define LEVEL_WARNING 1
#define LEVEL_INFO 2
#define LEVEL_DEBUG 3


#define TAG_GLOBAL 0
#define TAG_DETECTOR 1
#define TAG_WEAPON 2
#define TAG_PORTEXPANDER 3
#define TAG_WIFI 4
#define TAG_RADIO 5
#define TAG_GAMELOOP 6
#define TAG_SETUP 7
#define TAG_PLAYER 8
#define TAG_SCORE 9
#define TAG_LED_HANDLER 10
#define TAG_SOUND 11
#define TAG_TEST 12



namespace debug {

  class DebugHandler {

  public:
    DebugHandler();
    String handleDebugCommand(char* command);

    /**
    Logs to serial and telnet under consideration of current debug configuration
    */
    void log(uint level, uint tag, String msg);

    void inline info(uint tag, String msg){
      log(LEVEL_INFO,tag,msg);
    }
    void inline error(uint tag, String msg){
      log(LEVEL_ERROR,tag,msg);
    }
    void inline warn(uint tag, String msg){
      log(LEVEL_WARNING,tag,msg);
    }

    void inline debug(uint tag, String msg){
      log(LEVEL_DEBUG,tag,msg);
    }

    ///Should only be called by wifi handler to provide telnet output
    void setTelnetOutput(void (*printTelnet)( String));

    /**
    Should be called after Serial has been setup
    @param enable_serial If we should log to Serial
    @param enable_tcp If we should log to TCP/Telnet
    @param default_level Only log messages above this level (lower id) will be processed. See debug_handler.h for definitions
    @param default_tags Interpretet bitwise. Only logs for categories with enabled bit will be processed. See debug_handler.h for offset definitions
    */
    void init(bool enable_serial, bool enable_tcp, uint default_level,uint default_tags);

    /**
    Provide references for interaction
    */
    void configure(entity::Player *player, entity::Weapon *weapon_);

  private:
    void (*printTelnet)(String);
    uint enabled_tags;
    uint log_level;
    bool enable_tcp;
    bool enable_serial;
    String tagIdToString(uint id);
    entity::Player *player = 0;
    entity::Weapon *weapon_ = 0;

  };

  extern DebugHandler DH;
}
#endif
