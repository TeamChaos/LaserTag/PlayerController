#ifndef WEAPON_DESC_H
#define WEAPON_DESC_H

namespace data {
  struct WeaponDesc {
    int damage;  //Damage. Is rounded to multiples of four. Exception 1 -> 1 and -1 -> infinite
    int delay; //Fire delay in ms. Rounded to multiples of 40. Lowest reasonable value is currently 320ms. (Max 10000)
    bool fullauto; //If full auto or semi-auto.
    int magsize; //Number of shots per magazine.
    int initial_mags;  //Number of initial magazines or (-1) infinite.
    int reload_time; //Reload time in ms. Rounded to multiples of 80. (Max 20000)
    WeaponDesc(int damage_, int delay_, bool fullauto_, int magsize_, int initial_mags_, int reload_time_) : damage(damage_), delay(delay_), fullauto(fullauto_), magsize(magsize_), initial_mags(initial_mags_), reload_time(reload_time_) {}
  };
}

#endif
