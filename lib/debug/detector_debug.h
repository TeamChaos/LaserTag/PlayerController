#ifndef DETECTOR_DEBUG_H
#define DETECTOR_DEBUG_H

#include "../../CommonLib/detector_control/protocol.h"
#include "i2c_util.h"

namespace debug{
  ///We need to implement this externally so the protocol macro definitions do not conflict
  inline String handleDetectorDebugCommand(char* command){
    String output="";
    if(strncmp(command,"detector status",strlen("detector status"))==0){
      uint8_t response;
      byte result = util::i2c::read(DETECTOR_TWI_ADDRESS, I2C_STATUS_GET,&response);
      if(result!=0){
        output+="Failed to read status. Result: "+util::i2c::byteToString(result)+"\n";
      }
      else{
        output+="Status: "+util::i2c::byteToString(response)+"\n";
      }
    }
    else if(strncmp(command,"detector pause",strlen("detector pause"))==0){
      byte result= util::i2c::write(DETECTOR_TWI_ADDRESS,(uint)I2C_STATUS_SET,(uint)STATUS_PAUSED);
      if(result!=0){
        output+="Failed to pause detector. Result: "+util::i2c::byteToString(result)+"\n";
      }
      else{
        output+="Ok\n";
      }
    }
    else if(strncmp(command,"detector continue",strlen("detector continue"))==0){
      byte result= util::i2c::write(DETECTOR_TWI_ADDRESS,(uint)I2C_STATUS_SET,(uint)STATUS_DETECTING);
      if(result!=0){
        output+="Failed to continue detecor. Result: "+util::i2c::byteToString(result)+"\n";
      }
      else{
        output+="Ok\n";
      }
    }
    else if(strncmp(command, "detector info",strlen("detector info"))==0){
      uint8_t errors;
      uint8_t i2c_errors;

      byte result=util::i2c::read(DETECTOR_TWI_ADDRESS, I2C_ERRORS_GET,&errors);
      result |= util::i2c::read(DETECTOR_TWI_ADDRESS, I2C_ERRORS_I2C_GET,&i2c_errors);

      if(result!=0){
        output+="Failed to read info\n";
      }
      else{
        output+="Errors: "+String(errors)+" I2C Errors: "+String(i2c_errors)+"\n";
      }

    }
    else{
      output+="Usage: detector status/pause/continue/info\n";
    }
    return output;
  }

  inline String getDetectorGitVersion(){
    uint8_t buffer[8];
    byte result=util::i2c::read(DETECTOR_TWI_ADDRESS, I2C_READ_VERSION1,buffer,8);
    if(result!=0){
      return "FAILED!";
    }
    return String((char*)buffer);
  }
}
#endif //DETECTOR_DEBUG_H
