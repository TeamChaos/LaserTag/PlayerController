#ifndef WEAPON_DEBUG_H
#define WEAPON_DEBUG_H

#include "../../CommonLib/weapon_control/protocol.h"
#include "i2c_util.h"

namespace debug{
  ///We need to implement this externally so the protocol macro definitions do not conflict
  inline String handleWeaponDebugCommand(char* command){
    String output="";
    if(strncmp(command,"weapon status",strlen("weapon status"))==0){
      uint8_t response;
      byte result = util::i2c::read(WEAPON_I2C_ADDRESS, I2C_STATUS,&response);
      if(result!=0){
        output+="Failed to read status. Result: "+util::i2c::byteToString(result)+"\n";
      }
      else{
        output+="Status: "+util::i2c::byteToString(response)+"\n";
      }
    }
    else if(strncmp(command,"weapon enable",strlen("weapon enable"))==0){
      byte result= util::i2c::write(WEAPON_I2C_ADDRESS,I2C_ENABLE,1);
      if(result!=0){
        output+="Failed to enable weapon. Result: "+util::i2c::byteToString(result)+"\n";
      }
      else{
        output+="Ok\n";
      }
    }
    else if(strncmp(command,"weapon disable",strlen("weapon disable"))==0){
      byte result= util::i2c::write(WEAPON_I2C_ADDRESS,I2C_ENABLE,0);
      if(result!=0){
        output+="Failed to disable weapon. Result: "+util::i2c::byteToString(result)+"\n";
      }
      else{
        output+="Ok\n";
      }
    }
    else if(strncmp(command, "weapon info",strlen("weapon info"))==0){
      uint8_t enabled;
      uint8_t status;
      uint8_t mags;
      uint8_t shots_fired_lsb;
      uint8_t shots_fired_msb;
      uint8_t ammo;
      uint8_t id;
      byte result=util::i2c::read(WEAPON_I2C_ADDRESS, I2C_ENABLE,&enabled);
      result |= util::i2c::read(WEAPON_I2C_ADDRESS, I2C_STATUS,&status);
      result |= util::i2c::read(WEAPON_I2C_ADDRESS, I2C_MAGS,&mags);
      result |= util::i2c::read(WEAPON_I2C_ADDRESS, I2C_SHOTS_FIRED_LSB,&shots_fired_lsb);
      result |= util::i2c::read(WEAPON_I2C_ADDRESS, I2C_SHOTS_FIRED_MSB,&shots_fired_msb);
      result |= util::i2c::read(WEAPON_I2C_ADDRESS, I2C_AMMUNITION,&ammo);
      result |= util::i2c::read(WEAPON_I2C_ADDRESS, I2C_WEAPON_ID,&id);
      if(result!=0){
        output+="Failed to read info\n";
      }
      else{
        output+="Enabled: "+String(enabled)+" Status: "+util::i2c::byteToString(status)+"ID: "+String(id)+" Mags: "+String(mags)+" Ammo: "+String(ammo)+" ShotsFired: "+String(shots_fired_msb<<8|shots_fired_lsb)+"\n";
      }

    }
    else{
      output+="Usage: weapon status/enable/disable/info\n";
    }
    return output;
  }

  inline String getWeaponGitVersion(){
    uint8_t buffer[8];
    byte result=util::i2c::read(WEAPON_I2C_ADDRESS, I2C_READ_VERSION1,buffer,8);
    if(result!=0){
      return "FAILED!";
    }
    return String((char*)buffer);
  }
}
#endif //WEAPON_DEBUG_H
