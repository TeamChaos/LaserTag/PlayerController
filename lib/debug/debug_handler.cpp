#include "debug_handler.h"
#include "i2c_util.h"
#include "weapon_debug.h"
#include "detector_debug.h"

using namespace debug;


DebugHandler debug::DH;

DebugHandler::DebugHandler(){
  enable_serial=false;
  enable_tcp=false;
}

void DebugHandler::init(bool enable_serial, bool enable_tcp, uint default_level,uint default_tags){
  this->enable_serial=enable_serial;
  this->enable_tcp=enable_tcp;
  this->log_level=default_level;
  this->enabled_tags=default_tags;
  Serial.printf("Set up debugging handler with Serial: %u TCP: %u and level %u and tags 0x%04X \n",enable_serial,enable_tcp,default_level,default_tags);
}

String DebugHandler::handleDebugCommand(char* command){
  Serial.println(command);
  String output="";

  if(strncmp(command,"info",strlen("info"))==0){
    output+="OK\n";
  }
  else if(strncmp(command,"i2c",strlen("i2c"))==0){
    if(strncmp(command,"i2c get",strlen("i2c get"))==0){
      uint arg1;
      uint arg2;
      uint arg3;

      int args = sscanf(command,"i2c get 0x%x 0x%x %u",&arg1,&arg2, &arg3);
      if(args < 2){
        args = sscanf(command,"i2c get %u %u %u",&arg1,&arg2, &arg3);
      }
      if(args < 2){
        output+="Usage: i2c get <i2c-id> <address> [<amount>]\n";
      }
      else{
        if(args <3){
          arg3=1;
        }
        uint8_t buffer[arg3];
        byte result=util::i2c::read(arg1,arg2,buffer,arg3);
        if(result!=0){
          output+="Failed to read. Result: "+util::i2c::byteToString(result)+"\n";
        }
        else{
          output+="Response: ";
          for(uint i=0;i<arg3;i++){
            if(i>0){
              output+=":";
            }
            output+=util::i2c::byteToString(buffer[i]);
          }
          output+="\n";
        }

      }
    }
    else if(strncmp(command,"i2c set",strlen("i2c set"))==0){
      uint arg1;
      uint arg2;
      uint arg3;

      int args = sscanf(command,"i2c set 0x%x 0x%x 0x%x",&arg1,&arg2,&arg3);
      if(args <3){
        args = sscanf(command,"i2c set %u %u %u",&arg1,&arg2,&arg3);
      }
      if(args <3){
        output+="Usage: i2c set <i2c-id> <address> <value>\n";
      }
      else{
        byte result=util::i2c::write(arg1,arg2,arg3);
        if(result!=0){
          output+="Failed to write address. Result: "+util::i2c::byteToString(result)+"\n";
        }
        else{
          output+="Ok\n";
        }
      }
    }
    else{
      output+="Usage: i2c get/set\n";
    }

  }
  else if(strncmp(command,"weapon",strlen("weapon"))==0){
    output+=handleWeaponDebugCommand(command);//We need to implement this externally so the protocol macro definitions do not conflict
  }
  else if(strncmp(command,"detector",strlen("detector"))==0){
    output+=handleDetectorDebugCommand(command);
  }
  else if(strncmp(command,"version",strlen("version"))==0){
    output+="Weapon: "+getWeaponGitVersion()+"\n";
    output+="Detector: "+getDetectorGitVersion()+"\n";
  }
  else if(strncmp(command,"log_tags",strlen("log_tags"))==0){
    uint arg1;
    int args=sscanf(command,"log_tags 0x%x", &arg1);
    if(args <1){
      output+="Usage: log_tags <tag-byte>\n";
      output+="<Setup-Gameloop-Portexpander-Wifi-Weapon-Detector-Global>\n";
    }
    else{
      enabled_tags=arg1;
      output+="Ok\n";
    }
  }
  else if(strncmp(command,"log_level",strlen("log_level"))==0){
    uint arg1;
    int args=sscanf(command,"log_level 0x%x", &arg1);
    if(args <1){
      output+="Usage: log_level <level>\n";
    }
    else{
      log_level=arg1;
      output+="Ok\n";
    }
  }
  else if(strncmp(command,"player",strlen("player"))==0){
    if(strncmp(command,"player respawn",strlen("player respawn"))==0){
      if(player!=0 && weapon_!=0){
        player->respawn(0);
        weapon_->configure(player->getID(), player->getWeapon()); //TODO add some kind of reset enable method
        weapon_->enable();
        output+="Respawn\n";
      }
    }
    else if(strncmp(command,"player get health",strlen("player get health"))==0){
      if(player!=0){
        output+="Health: "+String(player->getHealth())+ "\n";
      }
    }
  }


  return output;
}

void DebugHandler::setTelnetOutput(void (*printTelnet)( String)){
  Serial.println("Setup telnet output");

  this->printTelnet=printTelnet;
}


void DebugHandler::log(uint level, uint tag, String msg){
  if(level<=this->log_level){
    if((1 << tag) & this->enabled_tags){
      String prefix="";
      switch(level){
        case LEVEL_ERROR:
          prefix="[E]";
          break;
        case LEVEL_WARNING:
          prefix="[W]";
          break;
        default:
          break;
      }
      String output=prefix+tagIdToString(tag)+msg;
      if(this->enable_serial){
        Serial.println(output);
      }
      if(this->enable_tcp && printTelnet !=NULL){
        printTelnet(output+"\n");
      }
    }
  }
}

void DebugHandler::configure(entity::Player *player, entity::Weapon *weapon_){
  this->player = player;
  this->weapon_ = weapon_;
}


String DebugHandler::tagIdToString(uint id){
  switch(id){
    case TAG_GLOBAL:
      return "[Glob]";
    case TAG_DETECTOR:
      return "[Detec]";
    case TAG_WEAPON:
      return "[Weap]";
    case TAG_PORTEXPANDER:
      return "[Portexp]";
    case TAG_WIFI:
      return "[Wifi]";
    case TAG_RADIO:
      return "[Radio]";
    case TAG_GAMELOOP:
      return "[Game]";
    case TAG_SETUP:
      return "[Setup]";
    case TAG_PLAYER:
      return "[Player]";
    case TAG_SCORE:
      return "[Score]";
    case TAG_SOUND:
      return "[Sound]";
    case TAG_TEST:
      return "[Test]";
    default:
      return "[]";
  }
}