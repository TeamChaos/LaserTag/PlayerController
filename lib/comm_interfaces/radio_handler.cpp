#include "radio_handler.h"
#include "debug_handler.h"
#include "Si446x.h"


//definition of radio signals as numeric values
//maybe for better readability but neglected when numeric values of these signals
// directly correspond to order in signal type enumeration (see header file)
//test

#define ID_KILL 0
#define ID_READY 1
#define ID_START 2
#define ID_STOP 3
#define ID_RESPAWN 6


#define CHANNEL 5
#define MAX_PACKET_SIZE 50


using namespace comm_interfaces;

// RCSwitch rcs; ///< Handles radio communication

#define PACKET_NONE		0
#define PACKET_OK		1
#define PACKET_INVALID	2

typedef struct{
	uint8_t ready;
	uint32_t timestamp;
	int16_t rssi;
	uint8_t length;
	uint8_t buffer[MAX_PACKET_SIZE];
} radioData_t;

static volatile radioData_t radioData;


RadioHandler::RadioHandler(comm_interfaces::SPIHandler *spi_, int deviceID_ ) : spi(spi_), deviceID(deviceID_) {
    Si446x_init(spi_);
    Si446x_setTxPower(22);
    Si446x_RX(CHANNEL);
}

void SI446X_CB_RXCOMPLETE(uint8_t length, int16_t rssi)
{
	if(length > MAX_PACKET_SIZE)
		length = MAX_PACKET_SIZE;

	radioData.ready = PACKET_OK;
	radioData.rssi = rssi;
	radioData.length = length;

	Si446x_read((uint8_t*)radioData.buffer, length);

	// Radio will now be in idle mode
}

void SI446X_CB_RXINVALID(int16_t rssi)
{
	radioData.ready = PACKET_INVALID;
	radioData.rssi = rssi;
}


void RadioHandler::tick() {

  if(radioData.ready!=PACKET_NONE){
    if(radioData.ready==PACKET_INVALID){
      debug::DH.warn(TAG_RADIO,"Received invalid signal");
    }
    else{
      decodeRadioSignal();
    }
    radioData.ready=PACKET_NONE;
    Si446x_RX(CHANNEL);
  }
}


void RadioHandler::decodeRadioSignal() {
  debug::DH.debug(TAG_RADIO,"Received message id: "+String(radioData.buffer[0]));
  switch(radioData.buffer[0]){
    case ID_KILL:
      if(radioData.length!=14){
        debug::DH.error(TAG_RADIO,"Received illegal kill message. Length: "+String(radioData.length));
      }
      else{
        receivedScoreMsg.offenderID = (radioData.buffer[1]&0b1111000) >>4;
        receivedScoreMsg.victimID = radioData.buffer[1]&0b1111;
        receivedScoreMsg.kills = radioData.buffer[2];
        receivedScoreMsg.deaths = radioData.buffer[3];
        for(int i=0;i<10;i++){ //Copy the assit values
          receivedScoreMsg.assists[i]=radioData.buffer[i+4];
        }
        scoreMsgReceived=true;     
      }
      break;
    case ID_READY:
      finishSetup=true;
      break;
    case ID_START:
      startLaunchTimer = true;
      break;
    case ID_STOP:
      stopGame = true;
      break;
    default:
      debug::DH.warn(TAG_RADIO,"Received unknown message id: "+String(radioData.buffer[0]));
      break;
  }

}




void RadioHandler::transmitScoreMessage(game_analysis::ScoreMessage scoreMsg) {
  uint8_t data[14] = {0};
  data[0]=ID_KILL;
  data[1]=((scoreMsg.offenderID & 0b1111) << 4 ) | (scoreMsg.victimID & 0b1111);
  data[2]=scoreMsg.kills;
  data[3]=scoreMsg.deaths;
  memcpy(data+4,scoreMsg.assists,10);

  Si446x_TX(data,sizeof(data),CHANNEL,SI446X_STATE_RX);
}


void RadioHandler::transmitRespawnMessage() {
  uint8_t data[2] = {0};
  data[0]=ID_RESPAWN;
  data[1]=deviceID;

  Si446x_TX(data,sizeof(data),CHANNEL,SI446X_STATE_RX);
}

void RadioHandler::transmitConquestPointMesssage(int pointID){
  //NOP for now
}

void RadioHandler::transmitGamePoints(int gamePoints) {
  //Don't send point updates to keep the frequency clear (for now)
}

void RadioHandler::transmitDamageMessage(int offenderID, int damage, int type){
  //NOP 
}
