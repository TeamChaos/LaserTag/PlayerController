#ifndef IRHANDLER_H
#define IRHANDLER_H



namespace ir {

  /// definition of ir signal types
  enum IRSignalType {HIT, MEDKIT, AMMUNITION, SPAWNPOINT, CAPTUREPOINT, GRENADE, UNKOWN};

  //container for an ir signal
  struct IRSignal {
    IRSignalType type; //signal type

    int quantity; // use as store for damage, ammo, med, damage etc

    int offenderID; // ID of shooter in case of HIT type

    int head; // if the head receivers were hit, includes direction information LSB->MSB: left,back,right,front. Only valid in case of HIT type

    int body;  // if the body receivers were hit, includes direction information LSB->MSB, back, front. Only valid in case of HIT type

    int rawReceivers; //The raw reeceiver mapping
    //todo all other required information
    //...
  };



}
namespace comm_interfaces {

  class IRHandler {
  public:
    IRHandler(int receivePin);

    /// retrieving received Signal from attiny
    bool getSignal(ir::IRSignal &sig);

    /// Gets status of detector. Returns 0xFF if communication failed
    uint checkStatus();

    void startDetecting();

  private:
    /// Returns true if Detector has received signal
    bool signalReceived();

    ///  Converts raw ir signal to IRSignal data structure
    ir::IRSignal analyzeRawSignal(uint8_t id,  uint8_t data, uint16_t receivers);

  };
} //namespace
#endif
