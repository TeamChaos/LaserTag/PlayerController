#ifndef WIFIHANDLER_H
#define WIFIHANDLER_H
#include <Arduino.h>
#include <WiFiClientSecure.h>
#include <ArduinoWebsockets.h>

#include "game_com_interface.h"
#include "game_setup.h"
#include "score_tracker.h"

#include "../../include/config_secret.h"

#ifdef CONFIG_HOST_INSECURE
#define WIFI_CLIENT WiFiClient
#else
#define WIFI_CLIENT WiFiClientSecure
#endif



namespace comm_interfaces {

  class WifiHandler : public GameComInterface {

  public:
    WifiHandler(const char *masterSSID, const char *masterPW, int deviceID, bool useMobileData);

    /// Returns true if device connected to any base station WIFI network
    bool connected();

    /** Connect to base station WIFI network.
    * Connection attempt lasts for the time specified by CONNECTION_TIMEOUT
    * Pause during connection attempts lasts for the time specified by RECONNECTION_INTERVAL
    */
    void connect();


    /// Request game setup from base station via http
    bool requestGameSetup(int myPlayerID, game_management::gameSetup &setup);

    /// Get a setup to immediatly start the game
    void requestTestSetup(game_management::gameSetup &setup);

    /// Submit the final result via http
    bool submitResult(int myPlayerID, std::vector<game_analysis::scoreData>* data, std::vector<int>* hit_records);

    /// Notify base station that we are ready
    bool sendReady(int myPlayerID);

    /// Register to base station
    bool sendRegister(int myPlayerID);

    /// Starts websocket server
    void setupWebSocketServer();

    ///Send a debug message to any connected telnet client
    void TelnetMsg(String test);
    
    /// todo: make this private somehow
    void handleWebsocketMessage(websockets::WebsocketsMessage msg);

    void handleWebsocketEvent(websockets::WebsocketsEvent event, String data);

    ///Tick wifi handler to allow internal handlers to be executed
    void tick();

    //////// functions inheritated from game_com_interface.h

    /// Sends score message via websocket
    void transmitScoreMessage(game_analysis::ScoreMessage scoreMsg) override;

    /// Sends a game point update via websocket
    void transmitGamePoints(int gamePoints) override;

    /// Sends a respawn message via websocket
    void transmitRespawnMessage() override;

    void transmitConquestPointMesssage( int pointID) override;

    void transmitDamageMessage(int offenderID, int amount, int type) override;

    /**
     * If using mobile data check if the wifi is still connected. If not return false and initiate a reconnect.
     * Might take a short time, but does not block.
     */
    bool checkWiFiInGame();

    /**
     * Check if a reset has been requested via the network. Reset flag afterwards
     */
    bool isResetRequested();

  private:
    /// Convert string from http request to game setup types
    bool parseGameSetup(String httpResponse, game_management::gameSetup &setup);

    void parseWebsocketMessage(String msg);


    bool prepareClient(WIFI_CLIENT &client, const char* url,bool post);
    bool processResponseHeader(WIFI_CLIENT &client, bool expectData);

    void Telnet(); //Handle telnet

    websockets::WebsocketsClient webSocket; // client of main server's websocket

    const char *masterSSID; ///< ssid of base station WIFI network
    const char *masterPW; ///< password for base station WIFI network
    const char *host = CONFIG_HOST_DOMAIN; ///< host website where device retrieves game setup
    const uint16_t port = CONFIG_HOST_PORT; ///<port of host site

    const int deviceID; ///< id of this esp device

    bool useMobileData; ///< whether we use radio or mobile for ingame communication

    int reconnectStamp; ///< time stamp of last wifi connection attempt

    bool websocketInitialized = false;

    bool resetRequested = false;

    unsigned long wifi_reconnect_time = 0;

    /**
     * Connect to wifi (if not already connecting) but do not wait for connection to succeed 
     */
    void connectWithoutWait();


  };
}
#endif
