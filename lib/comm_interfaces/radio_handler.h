#ifndef RADIOHANDLER_H
#define RADIOHANDLER_H

#include <Arduino.h>

#include "game_com_interface.h"
#include "spi_handler.h"



namespace comm_interfaces {

  class RadioHandler : public GameComInterface {

  public:
    RadioHandler(comm_interfaces::SPIHandler *spi_, int deviceID_);

    /// Check for new signals
    void tick();


    /// Sends score message 
    void transmitScoreMessage(game_analysis::ScoreMessage scoreMsg) override;

    /// Sends gamepoints
    void transmitGamePoints(int gamePoints) override;

    /// Sends respawn message
    void transmitRespawnMessage() override;

    void transmitConquestPointMesssage( int pointID) override;

    //NOP
    void transmitDamageMessage(int offenderID, int amount, int type) override;

  private:


    /// Analyzes received signal and returns radioSignal
    void decodeRadioSignal();

    comm_interfaces::SPIHandler *spi;
    const int deviceID; ///< id of this esp device


  };
}
#endif
