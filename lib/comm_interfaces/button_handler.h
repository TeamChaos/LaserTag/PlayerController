#ifndef BUTTONHANDLER_H
#define BUTTONHANDLER_H

namespace comm_interfaces {

  class ButtonHandler {

  public:
    ButtonHandler(int button1_pin, int button2_pin, int button3_pin);
    void tick();
    bool hasBtn1BeenPressed();
    bool hasBtn2BeenPressed();
    bool hasBtn3BeenPressed();
    /// Make sure all internal button press caches are cleared
    void clearButtonCache();
    bool isReset();

  private:
    int button1_pin;
    int button2_pin;
    int button3_pin;

    int btn1_counter;
    int btn2_counter;
    int btn3_counter;
    bool btn1;
    bool btn2;
    bool btn3;
  };

}
#endif
