#include "button_handler.h"
#include <Arduino.h>


#define BUTTON_DEBOUNCE_TIMER 10000;
using namespace comm_interfaces;

ButtonHandler::ButtonHandler(int button1_pin, int button2_pin, int button3_pin) : button1_pin(button1_pin), button2_pin(button2_pin), button3_pin(button3_pin) {
  pinMode(button1_pin,INPUT_PULLUP);
  pinMode(button2_pin,INPUT_PULLUP);
  pinMode(button3_pin,INPUT_PULLUP);
  btn1=false;
  btn2=false;
  btn3=false;
  btn1_counter = BUTTON_DEBOUNCE_TIMER;
  btn2_counter = BUTTON_DEBOUNCE_TIMER;
  btn3_counter = BUTTON_DEBOUNCE_TIMER;

}

  void ButtonHandler::tick(){

    if(btn1_counter>0){
      btn1_counter--;
    }
    else{
      if(!digitalRead(button1_pin)){
        btn1_counter=BUTTON_DEBOUNCE_TIMER;
        btn1=true;
      }
    }

    if(btn2_counter>0){
      btn2_counter--;
    }
    else{
      if(!digitalRead(button2_pin)){
        btn2_counter=BUTTON_DEBOUNCE_TIMER;
        btn2=true;
      }
    }

    if(btn3_counter>0){
      btn3_counter--;
    }
    else{
      if(!digitalRead(button3_pin)){
        btn3_counter=BUTTON_DEBOUNCE_TIMER;
        btn3=true;
      }
    }

  }

  bool ButtonHandler::hasBtn1BeenPressed(){
    if(btn1){
      btn1=false;
      return true;
    }
    return false;
  }
  
  bool ButtonHandler::hasBtn2BeenPressed(){

    if(btn2){
      btn2=false;
      return true;
    }
    return false;

  }

  bool ButtonHandler::hasBtn3BeenPressed(){

    if(btn3){
      btn3=false;
      return true;
    }
    return false;

  }

  void ButtonHandler::clearButtonCache(){
    btn1 = btn2 = btn3 = false;
  }

  bool ButtonHandler::isReset(){
    return (!digitalRead(button1_pin) && !digitalRead(button2_pin) && !digitalRead(button3_pin));
  }
