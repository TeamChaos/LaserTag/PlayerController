#include "spi_handler.h"
#include <Arduino.h>
#include <SPI.h>
#include "Si446x.h"

using namespace comm_interfaces;


SPIClass* spiDev = NULL;
static const int spiClk = 1000000; // 1 MHz

SPIHandler::SPIHandler(int radioCS_, int soundCS_): radioCS(radioCS_), soundCS(soundCS_) {
    pinMode(radioCS_,OUTPUT);
    pinMode(soundCS,OUTPUT);
    digitalWrite(radioCS,HIGH);
    digitalWrite(soundCS,HIGH);
    spiDev = new SPIClass(VSPI);
    spiDev->begin();
}

uint8_t SPIHandler::transferRadio(uint8_t data){
    return spiDev->transfer(data);
}

void SPIHandler::beginRadio(){
  spiDev->beginTransaction(SPISettings(spiClk,MSBFIRST,SPI_MODE0));
    digitalWrite(radioCS,LOW);
}

void SPIHandler::endRadio(){
  digitalWrite(radioCS,HIGH);
  spiDev->endTransaction();
}

void SPIHandler::writeSound(int length, uint8_t* data){
  SI446X_NO_INTERRUPT(){
    //use it as you would the regular arduino SPI API
    spiDev->beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE3));
    digitalWrite(soundCS, LOW); //pull SS slow to prep other end for transfer
    spiDev->transfer(data,length);
    digitalWrite(soundCS, HIGH); //pull ss high to signify end of data transfer
    spiDev->endTransaction();
  }
}
