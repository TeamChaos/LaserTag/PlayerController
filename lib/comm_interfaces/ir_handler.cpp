
#include <limits>
#include <Arduino.h>
#include "ir_handler.h"
#include "i2c_util.h"
#include "../../CommonLib/detector_control/protocol.h"
#include "../../CommonLib/signals.h"
#include "debug_handler.h"


#define RECEIVER_MASK_HEAD_L 0b1
#define RECEIVER_MASK_HEAD_B 0b10
#define RECEIVER_MASK_HEAD_R 0b100
#define RECEIVER_MASK_HEAD_F 0b1000
#define RECEIVER_MASK_BODY_F 0b0011110000
#define RECEIVER_MASK_BODY_B 0b1100000000

#define RECEIVER_MASK_2_BODY_L 0b0001000000
#define RECEIVER_MASK_2_BODY_R 0b1000000000
#define RECEIVER_MASK_2_BODY_F 0b0100110000
#define RECEIVER_MASK_2_BODY_B 0b0010000000



using namespace comm_interfaces;
using namespace ir;

int receivePin;

IRHandler::IRHandler(int receivePinArg) {

  pinMode(receivePinArg,INPUT);
  receivePin=receivePinArg;
}

bool IRHandler::signalReceived() {
  return digitalRead(receivePin)==HIGH;
}

bool IRHandler::getSignal(IRSignal &sig) {
  if (!signalReceived()) return false;

  uint8_t results[6];

  byte status = util::i2c::read(DETECTOR_TWI_ADDRESS,I2C_GET_ALL,results,6);
  if(status!=0){
    debug::DH.error(TAG_DETECTOR,"Failed to write address. Result: "+String(status,HEX));
    return false;
  }

  if(results[0]==I2C_ALL_OK && results[5] == I2C_ALL_OK){
    uint8_t id = results[1] & 0b11111; //TODO check parity bit
    uint8_t data = results[2] & 0b11111;
    uint16_t receivers = (results[4] << 7) | results[3]; //MSB does not describe a receiver and should always be zero, so it can be ignored
    sig = analyzeRawSignal(id,data,receivers);
    return true;
  }
  else if(results[0]==I2C_ERROR_BYTE && results[5] == I2C_ERROR_BYTE){
    debug::DH.error(TAG_DETECTOR,"The receiver is in an invalid state or there have been other issues");
    return false;
  }
  else{
    debug::DH.error(TAG_DETECTOR,"There have been issues with the I2C transmission");
    return false;
  }
}


IRSignal IRHandler::analyzeRawSignal(uint8_t id,  uint8_t data, uint16_t receivers) {
  IRSignal decodedSignal;
  for(int i=0; i<10;i++){
    if(receivers & (0b1<<i)){
      debug::DH.info(TAG_DETECTOR,"Detected Receiver "+String(i));
    }
  }
  if(id & 0b10000){//Player
    decodedSignal.type = HIT;
    decodedSignal.offenderID = id & 0b1111;
    decodedSignal.quantity = data== SIGNAL_DATA_MIN_DAMAGE ? 1 : (data==SIGNAL_DATA_MAX_DAMAGE ? 100000 : data * SIGNAL_DATA_DAMAGE_MULT );
    decodedSignal.head = ((receivers & RECEIVER_MASK_HEAD_L)?0b1:0b0) | ((receivers & RECEIVER_MASK_HEAD_B)?0b10:0b0) | ((receivers & RECEIVER_MASK_HEAD_R)?0b100:0b0) | ((receivers & RECEIVER_MASK_HEAD_F)?0b1000:0b0);
    decodedSignal.body = ((receivers & RECEIVER_MASK_BODY_B)?0b1:0b0) | ((receivers & RECEIVER_MASK_BODY_F)?0b10:0b0);
    decodedSignal.rawReceivers = receivers;

  }
  else{
    switch (id) {
      case SIGNAL_ID_AMMO:
        decodedSignal.type = AMMUNITION;
        decodedSignal.quantity = data;
        break;
      case SIGNAL_ID_MEDKIT:
        decodedSignal.type = MEDKIT;
        decodedSignal.quantity = data == SIGNAL_DATA_MIN_HEALTH ? 1 : (data == SIGNAL_DATA_MAX_HEALTH ? std::numeric_limits<int>::max() : data * SIGNAL_DATA_HEALTH_MULT);
        break;
      case SIGNAL_ID_SPAWNPOINT:
        decodedSignal.type = SPAWNPOINT;
        decodedSignal.quantity = data == 0b11111 ? -1 : data;
        break;
      case SIGNAL_ID_CAPTUREPOINT:
        decodedSignal.type = CAPTUREPOINT;
        decodedSignal.quantity = data;
        break;
      case SIGNAL_ID_GRENADE:
        decodedSignal.type = GRENADE;
        decodedSignal.quantity = data;
        break;
    }
  }
  return decodedSignal;

}

void IRHandler::startDetecting(){
  byte status = util::i2c::write(DETECTOR_TWI_ADDRESS,I2C_STATUS_SET,(uint8_t)STATUS_DETECTING);
  if(status!=0){
    debug::DH.error(TAG_DETECTOR, "Failed to start detector. Status: "+String(status,HEX));
  }
}

uint IRHandler::checkStatus(){
  uint8_t buffer;
  byte status = util::i2c::read(DETECTOR_TWI_ADDRESS,I2C_STATUS_GET,&buffer);
  if(status!=0){
    debug::DH.error(TAG_DETECTOR, "Failed to query detector. Status: "+String(status,HEX)); //Status 2: NACK on address, 3: NACK on Data, 4: SDA pulled low
    return 0xFF;
  }
  return buffer;
}
