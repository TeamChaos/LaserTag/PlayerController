#include <WiFi.h>
#include <ArduinoJson.h>
#include "wifi_handler.h"
#include <WiFiClientSecure.h>
#include <WiFiClient.h>
#include "debug_handler.h"
#include "weapons.h"


//Wifi Configuration
#define RECONNECT_INTERVAL 5000 // wait 5 ms between each wifi connection attempt
#define CONNECTION_TIMEOUT 8000 //cancel connection attempt after 8 seconds of failure

//Telnet Configuration
#define INPUT_SIZE 50
#define MAX_TELNET_CLIENTS 3

const char * certificate PROGMEM = R"EOF(
   -----BEGIN CERTIFICATE-----
MIIDSjCCAjKgAwIBAgIQRK+wgNajJ7qJMDmGLvhAazANBgkqhkiG9w0BAQUFADA/
MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT
DkRTVCBSb290IENBIFgzMB4XDTAwMDkzMDIxMTIxOVoXDTIxMDkzMDE0MDExNVow
PzEkMCIGA1UEChMbRGlnaXRhbCBTaWduYXR1cmUgVHJ1c3QgQ28uMRcwFQYDVQQD
Ew5EU1QgUm9vdCBDQSBYMzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB
AN+v6ZdQCINXtMxiZfaQguzH0yxrMMpb7NnDfcdAwRgUi+DoM3ZJKuM/IUmTrE4O
rz5Iy2Xu/NMhD2XSKtkyj4zl93ewEnu1lcCJo6m67XMuegwGMoOifooUMM0RoOEq
OLl5CjH9UL2AZd+3UWODyOKIYepLYYHsUmu5ouJLGiifSKOeDNoJjj4XLh7dIN9b
xiqKqy69cK3FCxolkHRyxXtqqzTWMIn/5WgTe1QLyNau7Fqckh49ZLOMxt+/yUFw
7BZy1SbsOFU5Q9D8/RhcQPGX69Wam40dutolucbY38EVAjqr2m7xPi71XAicPNaD
aeQQmxkqtilX4+U9m5/wAl0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNV
HQ8BAf8EBAMCAQYwHQYDVR0OBBYEFMSnsaR7LHH62+FLkHX/xBVghYkQMA0GCSqG
SIb3DQEBBQUAA4IBAQCjGiybFwBcqR7uKGY3Or+Dxz9LwwmglSBd49lZRNI+DT69
ikugdB/OEIKcdBodfpga3csTS7MgROSR6cz8faXbauX+5v3gTt23ADq1cEmv8uXr
AvHRAosZy5Q6XkjEGB5YGV8eAlrwDPGxrancWYaLbumR9YbK+rlmM6pZW87ipxZz
R8srzJmwN0jP41ZL9c8PDHIyh8bwRLtTcm1D9SZImlJnt1ir/md2cXjbDaJWFBM5
JDGFoqgCWjBH4d1QB7wCCZAA62RjYJsWvIjJEubSfZGL+T0yjWW06XyxV3bqxbYo
Ob8VZRzI9neWagqNdwvYkQsEjgfbKbYK7p2CNTUQ
-----END CERTIFICATE-----
)EOF";


using namespace comm_interfaces;
using namespace websockets;


//Telnet setup
WiFiServer TelnetServer(23);
WiFiClient TelnetClient[MAX_TELNET_CLIENTS];
bool telnet_connection_established;

bool isAPSetup;



//We need a non member function to be able to pass it to debug_handler. However we also need the member function and wifi handler instance to be able to execute the TelnetMsg.
//Therefore we have a global (but secret [no extern]) WifiHandler reference here which is assigned in the WifiHandler constructor
WifiHandler *localGlobalHandlerInstance;
void printToTelnet(String s){
  localGlobalHandlerInstance->TelnetMsg(s);
}
void webSocketMsg(WebsocketsMessage msg) {
  localGlobalHandlerInstance->handleWebsocketMessage(msg);
}

void webSocketEvent(WebsocketsEvent event, String data) {
  localGlobalHandlerInstance->handleWebsocketEvent(event,data);
}



WifiHandler::WifiHandler(const char *masterSSID_, const char *masterPW_, int deviceID, bool useMobileData)
  : masterSSID(masterSSID_), masterPW(masterPW_), deviceID(deviceID), useMobileData(useMobileData), reconnectStamp(0) {
    game_management::gameSetup setup;
    //const char* json = "{\"gameMode\":\"TeamDeathMatch\",\"friendlyFire\":true,\"players\":{\"0\":{\"name\":\"Player1\",\"id\":0,\"team\":0},\"1\":{\"name\":\"Player2\",\"id\":1,\"team\":1},\"9\":{\"name\":\"WeaponOnly\",\"id\":9,\"team\":2}},\"launchTrigger\":\"timer\",\"launchTime\":60000,\"stopTrigger\":\"scoreLimit\",\"scoreLimit\":5000,\"killMultiplier\":10,\"assistMultiplier\":5}";
    //fillGameSetup(json, setup);
    //debug::DH.info(TAG_WIFI,strcat("Parsed json ",setup.players.at(0).name));
    localGlobalHandlerInstance=this;
}

bool WifiHandler::connected() {
  return WiFi.status() == WL_CONNECTED;
}

void WifiHandler::connect() {

  //check if reconnect interval is exceeded
  if ((millis() - reconnectStamp) < RECONNECT_INTERVAL) return;
  reconnectStamp = millis();
  debug::DH.setTelnetOutput(NULL);
  TelnetServer.stop();

  if(isAPSetup){
    WiFi.softAPdisconnect(true);
  }
  else{
    WiFi.disconnect();
  }
  isAPSetup=false;

  debug::DH.info(TAG_WIFI,"Connecting to WIFI network. "+String(masterSSID)+" - "+String(masterPW));
  WiFi.persistent(false);
  WiFi.mode(WIFI_STA);
  WiFi.begin(masterSSID, masterPW);

  while (WiFi.status() != WL_CONNECTED) {
    //check if connection timeout exceeded
    if ((millis() - reconnectStamp) > CONNECTION_TIMEOUT) {
      reconnectStamp = millis();
      debug::DH.error(TAG_WIFI, "\nConnection failed");
      return;
    }
    delay(500); //no spam on serial monitor
    Serial.print(".");
  }
  delay(100);

  debug::DH.info(TAG_WIFI,"\nWIFI connected\n");
}

void WifiHandler::connectWithoutWait() {

  //check if reconnect interval is exceeded
  if ((millis() - reconnectStamp) < RECONNECT_INTERVAL) return;
  reconnectStamp = millis();

  if(WiFi.status()==WL_IDLE_STATUS){
    return;
  }
  if(WiFi.status()==WL_CONNECTED){
    return;
  }


  if(isAPSetup){
    WiFi.softAPdisconnect(true);
  }
  else{
    WiFi.disconnect();
  }
  isAPSetup=false;

  debug::DH.info(TAG_WIFI,"Connecting to WIFI network. "+String(masterSSID)+" - "+String(masterPW));
  WiFi.persistent(false);
  WiFi.mode(WIFI_STA);
  WiFi.begin(masterSSID, masterPW);

  debug::DH.info(TAG_WIFI,"\nWIFI connecting\n");
}



void WifiHandler::requestTestSetup(game_management::gameSetup &setup){
  setup.mode = game_management::FREE_FOR_ALL;
  setup.teamBased = false;
  setup.friendlyFire = true;
  setup.killMultiplier = 10;
  setup.assistMultiplier = 5;
  setup.playerCount = 10;
  setup.launchTrigger = game_management::START_TIMER;
  setup.launchTime = 15000;
  for (int i = 0; i< setup.playerCount; i++) {
    game_management::Player p;
    setup.players.push_back(p);
  }
  setup.maxHealth = 100;
  setup.minHealth = 0;
  setup.respawnDelay = 0;
  setup.defaultWeapon = &data::weapons::defaultWeapon;
  setup.respawnOffset = 3000;
  setup.localRespawn = true;
  setup.extendedUpdates = false;
  std::vector<int> tmp2(10);

  setup.teamScore =tmp2;
}

bool WifiHandler::prepareClient(WIFI_CLIENT &client, const char* url,bool post){
  debug::DH.info(TAG_WIFI,"Connecting to "+String(host));

  // client.setCACert(certificate);       

  if(!client.connect(host, port)){
    debug::DH.error(TAG_WIFI,"Failed to connect to host");
    return false;
  }
  debug::DH.debug(TAG_WIFI, "Requesting URL: "+String(url));
  client.println(String(post?"POST ":"GET ")  + url + " HTTP/1.1");
  client.println(String("Host: " )+ host);
  client.println("Authorization: Basic " ESP_API_AUTH);
  client.println("Connection: close");
  return true;
}

bool WifiHandler::processResponseHeader(WIFI_CLIENT &client, bool expectData){
  // Check HTTP status
  char status[32] = {0};
  client.readBytesUntil('\r', status, sizeof(status));
  if(strcmp(status, "HTTP/1.1 503 Service Unavailable")==0){
    debug::DH.warn(TAG_WIFI,"Game in unexpected state");
    return false;
  }
  else if(strcmp(status, "HTTP/1.1 400 Bad Request")==0){
    debug::DH.error(TAG_WIFI,"We sent illegal data");
    return false;
  }
  else if(strcmp(status, "HTTP/1.1 401 Unauthorized")==0){
    debug::DH.error(TAG_WIFI,"Unauthorized");
    return false;
  }
  else if(strcmp(status, "HTTP/1.1 204 No Content") == 0){
    if(expectData){
    debug::DH.error(TAG_WIFI,"We were expecting data, but server does not provide any");
    return false;
    }
  }
  else if (strcmp(status, "HTTP/1.1 200 OK") != 0 ) {
    debug::DH.error(TAG_WIFI,"Unexpected response: "+String(status));
    return false;
  }

  // Skip HTTP headers
  char endOfHeaders[] = "\r\n\r\n";
  if (!client.find(endOfHeaders)) {
    debug::DH.error(TAG_WIFI,String("Invalid response"));
    return false;
  }
  return true;
}

bool WifiHandler::requestGameSetup(int myPlayerID, game_management::gameSetup &setup) {

  WIFI_CLIENT client;// = createWiFiClient();

  if(!prepareClient(client,("/esp/get_setup?player_id="+String(myPlayerID)).c_str(),false))return false;
  client.println("");//End of headers
  delay(500);
  if(!processResponseHeader(client,true)){
    client.stop();
    debug::DH.warn(TAG_WIFI,"Could not get setup");
    return false;
  }

  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
   String line = client.readStringUntil('\r');
   // remove white spaces
   line.trim();
   // required line is json syntax
   if (line.startsWith("{\"result\":1}")) {
     //disconnect
     client.stop();
     return false;
   }
   if (line.startsWith("{")) {
     //disconnect
     client.stop();
     return parseGameSetup(line, setup);
   }
  }
  client.stop();
  return false;

}

bool WifiHandler::sendReady(int myPlayerID){
  WIFI_CLIENT client;
  if(!prepareClient(client,("/esp/set_ready?player_id="+String(myPlayerID)).c_str(),false))return false;
  client.println("");
  delay(500);

  if(processResponseHeader(client,false)){
    client.stop();
    return true;
  }
  else{
    client.stop();
    return false;
  }



}

bool WifiHandler::sendRegister(int myPlayerID){
  WIFI_CLIENT client;
  if(!prepareClient(client,("/esp/register?player_id="+String(myPlayerID)).c_str(),false))return false;
  client.println("");
  delay(500);

  if(processResponseHeader(client,false)) {
    client.stop();
    return true;
  }
  else{
    client.stop();
    return false;
  }



}


bool WifiHandler::submitResult(int myPlayerID,std::vector<game_analysis::scoreData>* local, std::vector<int>* hit_records){
  debug::DH.info(TAG_WIFI,String("Submitting result"));
  const int max_records=100;
  const int capacity =JSON_OBJECT_SIZE(2) + 10*JSON_ARRAY_SIZE(4) + 2*JSON_ARRAY_SIZE(10) + JSON_ARRAY_SIZE(max_records) /*for_records*/ + 23;
  StaticJsonDocument<capacity> doc;
  debug::DH.info(TAG_WIFI,String("Preparing result"));
  JsonArray scoreboard = doc.createNestedArray("scoreboard");
  for(uint i=0;i<local->size();i++){
      JsonArray entry = scoreboard.createNestedArray();
      entry.add(local->at(i).kills);
      entry.add(local->at(i).assists);
      entry.add(local->at(i).deaths);
      entry.add(local->at(i).gamePoints);
    }

  if(hit_records->size()>0){
    JsonArray records = doc.createNestedArray("hit_records");
    for(int i=0;i<hit_records->size()&&i<max_records;i++){
       records.add(hit_records->at(i));
    }
  }


  debug::DH.info(TAG_WIFI,String("Prepared result"));


   WIFI_CLIENT client;
   if(!prepareClient(client,("/esp/submit_result?player_id="+String(myPlayerID)).c_str(),true)) return false;
   client.println("Content-Type: application/json");
   client.print("Content-Length: ");
   client.println(measureJson(doc));
   client.println();//Empty line terminates headers
   debug::DH.info(TAG_WIFI,String("Submitting result"));

   serializeJson(doc,client);
   delay(500);

   if(processResponseHeader(client,false)){
      client.stop();
      debug::DH.info(TAG_WIFI,"Submitted result");
      return true;
   }
   else{
      client.stop();
      debug::DH.warn(TAG_WIFI,"Could not submit result");
      return false;
   }


}

bool WifiHandler::parseGameSetup(String httpResponse, game_management::gameSetup &setup) {
  //todo define protocol and retrieve data
  // Convert from String Object to char*.
  char json[httpResponse.length()+1];
  httpResponse.replace("\\", "");
  httpResponse.toCharArray(json, sizeof(json));

  const char* c = json;

  // Allocate JsonBuffer
  // Use https://arduinojson.org/v6/assistant/ to compute the capacity.
  const size_t capacity = 3*JSON_OBJECT_SIZE(2) + 10*JSON_OBJECT_SIZE(3)+JSON_OBJECT_SIZE(5) + JSON_OBJECT_SIZE(10) + JSON_OBJECT_SIZE(15) +  700; //For max player setup
  DynamicJsonDocument doc(capacity);

  auto error = deserializeJson(doc,c);

  if (error) {
    debug::DH.error(TAG_WIFI,"Parsing failed");
    debug::DH.error(TAG_WIFI,error.c_str());
    return false;
  }

  const char* gamemode = doc["gameMode"];
  if(strcmp(gamemode, "TeamDeathMatch")==0  ){
    setup.mode = game_management::TEAM_DEATH_MATCH;
    setup.teamBased = true;
  }
  else if(strcmp(gamemode, "Conquest")==0){
    setup.mode = game_management::CONQUEST;
    setup.teamBased = true;
    setup.conquestPointCount = doc["conquestPointCount"];
  }
  else if(strcmp(gamemode,"FreeForAll")==0){
    setup.mode = game_management::FREE_FOR_ALL;
    setup.teamBased = false;
  }
  else if(strcmp(gamemode,"Domination")==0){
    setup.mode = game_management::DOMINATION;
    setup.teamBased = true;
  }
  else{
    debug::DH.error(TAG_WIFI,"Unknown gamemode");
    return false;
  }

  setup.friendlyFire = doc["friendlyFire"];
  setup.killMultiplier = doc["killMultiplier"];
  setup.assistMultiplier = doc["assistMultiplier"];
  setup.gameModePoints = doc["modePoints"];
  const char* launchtrigger = doc["launchTrigger"];
  if(strcmp(launchtrigger,"time")==0){
    setup.launchTrigger = game_management::START_TIMER;
  }
  else if(strcmp(launchtrigger,"spawnpoint")==0){
    setup.launchTrigger = game_management::START_SPAWNPOINT;
  }

  setup.launchTime = doc["launchTime"];
  setup.launchTime *= 1000;

  setup.localRespawn = doc["localRespawn"];
  setup.respawnOffset = doc["respawnOffset"];
  setup.respawnOffset *=1000;
  setup.respawnDelay = doc["respawnDelay"];
  setup.respawnDelay *=1000;
  setup.extendedUpdates = doc["extendedUpdates"];

  const char* weapon = doc["weapon"];
  if(strcmp(weapon, "default")==0){
    setup.defaultWeapon = &data::weapons::defaultWeapon;
  }
  else if(strcmp(weapon,"instant")==0){
    setup.defaultWeapon = &data::weapons::instantWeapon;
  }
  else if(strcmp(weapon,"auto")==0){
    setup.defaultWeapon = &data::weapons::autoWeapon;
  }
  else if(strcmp(weapon, "lowDamage")==0){
    setup.defaultWeapon = &data::weapons::lowDamageWeapon;
  }
  else if(strcmp(weapon, "test")==0){
    setup.defaultWeapon = &data::weapons::testWeapon;
  }
  else{
    debug::DH.warn(TAG_WIFI,"Unknown weapon type"+String(weapon));
    setup.defaultWeapon = &data::weapons::defaultWeapon;
  }

  std::vector<game_management::Player> tmp(10);

  setup.players  = tmp;
  JsonObject players = doc["players"].as<JsonObject>();
  int count=0;
  for(JsonPair pair : players){
    JsonObject p  = pair.value().as<JsonObject>();
    const char* name = p["name"];
    int id = p["id"];
    if(id<0||id>9){
      debug::DH.info(TAG_WIFI,"Invalid id "+ id);
    }
    int team = p["team"];
    game_management::Player player;
    player.name=name;
    player.playerID=id;
    player.teamID=team;
    setup.players[id]=player;
    //setup.players.push_back(player);
    count++;
  }
  setup.playerCount=count;
  std::vector<int> tmp2(10);

  setup.teamScore =tmp2;

  debug::DH.info(TAG_WIFI,"Sucessfully received game setup. Gamemode: "+String(setup.mode)+" Players "+String(setup.playerCount));
  return true;

}

void WifiHandler::setupWebSocketServer() {

  //Start debug Telnet
  //debug::DH.debug(TAG_WIFI,"Starting Telnet server");
  // TelnetServer.begin();
  // TelnetServer.setNoDelay(true);

  // debug::DH.setTelnetOutput(printToTelnet);
  if(websocketInitialized)return;
  if(!connected())return;

  debug::DH.debug(TAG_WIFI,"ws begin");

	// event handler
	webSocket.onEvent(webSocketEvent);
  webSocket.onMessage(webSocketMsg);

  // server address, port and URL
  webSocket.addHeader("Authorization","Basic " ESP_API_AUTH);
  webSocket.connect(CONFIG_WS_LOCATION);
  webSocket.setInsecure();


	// try ever 5000 again if connection has failed
	//webSocket.setReconnectInterval(5000);

  websocketInitialized = true;
}

bool WifiHandler::checkWiFiInGame(){
  if(useMobileData){
    if(!connected()){
        if(millis() > wifi_reconnect_time){
            connectWithoutWait();
            wifi_reconnect_time = millis()+10000;
        }
        return false;
    }
    return true;
  }
  else{
    return true;
  }
}

void WifiHandler::handleWebsocketEvent(WebsocketsEvent event, String data){
    if(event == WebsocketsEvent::ConnectionOpened) {
        debug::DH.debug(TAG_WIFI,"Connnection Opened");
        // send message to server when Connected
			  webSocket.send("{\"id\":\"hello\",\"deviceID\":"+String(deviceID)+"}");
    } else if(event == WebsocketsEvent::ConnectionClosed) {
        debug::DH.debug(TAG_WIFI,"Connnection Closed "+String(data));
        debug::DH.debug(TAG_WIFI,String(webSocket.getCloseReason()));
    } else if(event == WebsocketsEvent::GotPing) {
        debug::DH.debug(TAG_WIFI,"Got a Ping!");
        webSocket.pong();
    } else if(event == WebsocketsEvent::GotPong) {
        debug::DH.debug(TAG_WIFI,"Got a Pong!");
    }
}

void WifiHandler::handleWebsocketMessage(WebsocketsMessage message) {

      String msg = message.data();
      debug::DH.debug(TAG_WIFI,"Received msg via websocket: " + msg);
      parseWebsocketMessage(msg);

}

void WifiHandler::parseWebsocketMessage(String msg) {
  const size_t capacity = JSON_ARRAY_SIZE(10) + JSON_OBJECT_SIZE(6) +  49; //For max player setup
  DynamicJsonDocument json(capacity);

  auto error = deserializeJson(json, msg);

  if (error) {
    debug::DH.error(TAG_WIFI,"Parsing failed");
    debug::DH.error(TAG_WIFI,error.c_str());
    return;
  }

  if (strcmp(json["id"],"ready")==0) 
    finishSetup = true;  

  else if(strcmp(json["id"] , "start")==0) {
    startLaunchTimer = true;
  }
  else if (strcmp(json["id"] , "kill")==0) {
    scoreMsgReceived=true;
    receivedScoreMsg.offenderID = json["offenderID"];
    receivedScoreMsg.victimID = json["victimID"];
    receivedScoreMsg.deaths = json["deaths"];
    receivedScoreMsg.kills = json["deaths"];
    // todo: parse assist array and store in receivedScoreMsg
    // JsonObject assists = json["assists"].as<JsonObject>();
  }
  else if(strcmp(json["id"] , "cpoint_captured")==0){
    JsonArray players = json["players"];
    for(int i=0;i<players.size();i++){
      if(players.getElement(i)==this->deviceID){
        cpoint_captured = json["teamID"];
        break;
      }
    }
  }
  else if (strcmp(json["id"] , "stop")==0) {
    debug::DH.info(TAG_WIFI,"Received stop message "+finishSetup);
    stopGame = finishSetup; //Only process stop message if the game has been setup before. Otherwise the flag will kill the game later on.
  }
  else if(strcmp(json["id"] , "refresh")==0) {
    //TODO
  }
  else if(strcmp(json["id"] , "reset")==0) {
    resetRequested = true;
  }

}

bool WifiHandler::isResetRequested(){
  if(resetRequested){
    resetRequested=false;
    return true;
  }
  return false;
}

void WifiHandler::transmitGamePoints(int gamePoints) {
  const int capacity =  JSON_OBJECT_SIZE(3) + 30;
    // allocate the memory for the document
  StaticJsonDocument<capacity> doc;
  
  doc["id"] = "gamepoints";
  doc["playerID"] = deviceID;
  doc["points"] = gamePoints;

  
  // serialize the array and send the result to Serial
  String jsonString;
  serializeJson(doc, jsonString);

  webSocket.send(jsonString);
}

void WifiHandler::transmitConquestPointMesssage(int pointID) {
  const int capacity =  JSON_OBJECT_SIZE(3) + 30;
    // allocate the memory for the document
  StaticJsonDocument<capacity> doc;
  
  doc["id"] = "cpoint";
  doc["playerID"] = deviceID;
  doc["pointID"] = pointID;

  
  // serialize the array and send the result to Serial
  String jsonString;
  serializeJson(doc, jsonString);

  webSocket.send(jsonString);
}

void WifiHandler::transmitDamageMessage(int offenderID, int amount, int type){
  const int capacity =  JSON_OBJECT_SIZE(5) + 48;
    // allocate the memory for the document
  StaticJsonDocument<capacity> doc;
  
  doc["id"] = "dmg";
  doc["playerID"] = deviceID;
  doc["offenderID"] = offenderID;
  doc["amount"] = amount;
  doc["type"] = type;

  
  // serialize the array and send the result to Serial
  String jsonString;
  serializeJson(doc, jsonString);

  webSocket.send(jsonString);
}

void WifiHandler::transmitRespawnMessage() {
  const int capacity =  JSON_OBJECT_SIZE(2) + 20;
    // allocate the memory for the document
  StaticJsonDocument<capacity> doc;
  
  doc["id"] = "respawn";
  doc["playerID"] = deviceID;
  
  // serialize the array and send the result to Serial
  String jsonString;
  serializeJson(doc, jsonString);

  webSocket.send(jsonString);
}

void WifiHandler::transmitScoreMessage(game_analysis::ScoreMessage scoreMsg) {
  const int capacity =  JSON_ARRAY_SIZE(10) + JSON_OBJECT_SIZE(6) + 48;
    // allocate the memory for the document
  StaticJsonDocument<capacity> doc;
  
  doc["id"] = "kill";
  doc["victimID"] = scoreMsg.victimID;
  doc["deaths"] = scoreMsg.deaths;
  doc["offenderID"] = scoreMsg.offenderID;
  doc["kills"] = scoreMsg.kills;

  JsonArray absoluteAssists = doc.createNestedArray("assists");

  for (int i = 0; i < 10; i++) {
    absoluteAssists.add(scoreMsg.assists[i]);
  }

  
  // serialize the array and send the result to Serial
  String jsonString;
  serializeJson(doc, jsonString);

  webSocket.send(jsonString);
}

void WifiHandler::tick(){
  if(isAPSetup)Telnet();
	if(websocketInitialized){
    webSocket.poll();
    if(!webSocket.available(false)){
      websocketInitialized=false;
    }
  }
  else{
    setupWebSocketServer();
  }
}


void WifiHandler::TelnetMsg(String text)
{
  if(!isAPSetup)return;
  for(int i = 0; i < MAX_TELNET_CLIENTS; i++)
  {
    if (TelnetClient[i] && TelnetClient[i].connected())
    {
      TelnetClient[i].print(text);
    }
  }
  delay(10);  // to avoid strange characters left in buffer
}


//General TELNET handler
void WifiHandler::Telnet()
{
  // Cleanup disconnected session
  for(int i = 0; i < MAX_TELNET_CLIENTS; i++)
  {
    if (TelnetClient[i] && !TelnetClient[i].connected())
    {
      Serial.print("Client disconnected ... terminate session "); Serial.println(i+1);
      TelnetClient[i].stop();
    }
  }

  // Check new client connections
  if (TelnetServer.hasClient())
  {
    telnet_connection_established = false; // Set to false

    for(int i = 0; i < MAX_TELNET_CLIENTS; i++)
    {
      // Serial.print("Checking telnet session "); Serial.println(i+1);

      // find free socket
      if (!TelnetClient[i])
      {
        TelnetClient[i] = TelnetServer.available();

        Serial.print("New Telnet client connected to session "); Serial.println(i+1);

        TelnetClient[i].flush();  // clear input buffer, else you get strange characters
        TelnetClient[i].println("Welcome!");

        TelnetClient[i].print("Millis since start: ");
        TelnetClient[i].println(millis());

        TelnetClient[i].print("Free Heap RAM: ");
        TelnetClient[i].println(ESP.getFreeHeap());

        TelnetClient[i].println("----------------------------------------------------------------");

        telnet_connection_established = true;

        break;
      }
      else
      {
        // Serial.println("Session is in use");
      }
    }

    if (telnet_connection_established == false)
    {
      debug::DH.warn(TAG_WIFI,"No free sessions ... drop connection");
      TelnetServer.available().stop();
      // TelnetMsg("An other user cannot connect ... MAX_TELNET_CLIENTS limit is reached!");
    }
  }

  for(int i = 0; i < MAX_TELNET_CLIENTS; i++)
  {
    if (TelnetClient[i] && TelnetClient[i].connected())
    {
      if(TelnetClient[i].available())
      {
        String output="";
        //get data from the telnet client
        while(TelnetClient[i].available())
        {
          char buffer[INPUT_SIZE + 1];
          byte size = TelnetClient[i].readBytesUntil('\n', buffer, INPUT_SIZE); //Probably should not use String because overhead
          buffer[size]=0;//Add 0 for c string end
          output+=debug::DH.handleDebugCommand(buffer);
        }
        TelnetClient[i].print(output);
      }
    }
  }
}
