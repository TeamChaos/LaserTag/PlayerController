#ifndef SPIHANDLER_H
#define SPIHANDLER_H

#include <stdint.h>

namespace comm_interfaces {

  class SPIHandler {

  public:
    SPIHandler(int radioCS_, int soundCS_);
    void writeSound(int count, uint8_t *data);
    uint8_t transferRadio(uint8_t data);
    void beginRadio();
    void endRadio();


  private:
    int radioCS;
    int soundCS;


  };
}
#endif
