#ifndef GAMECOMINTERFACE_H
#define GAMECOMINTERFACE_H

#include "score_tracker.h"

using namespace game_analysis;

namespace comm_interfaces {

    enum DamageType{
        SHOT, HEADSHOT, GRENADE, MEDKIT, CLAYMORE
    };
    
    class GameComInterface {

    public:

        // whether setup is finished
        bool setupFinished() { return finishSetup; };

        // whether master sent signal to start launch timer
        bool launchTimerStarted() { return startLaunchTimer; };

        // whether master sent signal to stop game
        bool gameStopped() { return stopGame; };

        bool getReceivedScoreMessage(ScoreMessage &receivedScoreMsg ) {
            if (scoreMsgReceived) {
                receivedScoreMsg = this->receivedScoreMsg;
                scoreMsgReceived = false;
                return true;
            }
            return false;            
        } ;

        int cPointCaptured(){
            int t= cpoint_captured;
            cpoint_captured=-1;
            return t;
        }

        // reset flags
        void reset() {
            scoreMsgReceived = false;
            startLaunchTimer = false;
            finishSetup = false;
            stopGame = false;
        }
        
        // Sends score message 
        virtual void transmitScoreMessage(ScoreMessage scoreMsg);

        // Sends game point message
        virtual void transmitGamePoints(int gamePoints);

        // Sends respawn message
        virtual void transmitRespawnMessage();

        // Sends a conquest point message
        virtual void transmitConquestPointMesssage( int pointID);

        // On data network this transmits a single damage message. offenderID can be -1 if unknown
        virtual void transmitDamageMessage(int offenderID, int damage, int type);

    protected:

        bool finishSetup = false; // true if master sent SETUP_FINISHED signal
        bool startLaunchTimer = false; // true if master sent signal to start launch timer
        bool stopGame = false; // true if master sent signal to stop game
        bool scoreMsgReceived = false; // true if received a new score message
        int cpoint_captured = -1; // != -1 if point captured. number indicates capturing team
        ScoreMessage receivedScoreMsg; // recently received scoreMsg
        

    };


}
#endif