#ifndef LEDCONTROLLER_H
#define LEDCONTROLLER_H

#include <stdint.h>

#define LED_STATUS_BLINK_FAST 1
#define LED_STATUS_BLINK_SLOW 2
#define LED_STATUS_ON 3
#define LED_STATUS_OFF 4

#define LED_RGB_VIOLET -4
#define LED_RGB_WHITE -3
#define LED_RGB_BLUE -2
#define LED_RGB_PINK -1
#define LED_RGB_OFF 0

namespace signal_output {

  class LedController {

  public:
    LedController();

    /**
    Turn off all LEDs
    **/
    void turnOff();

    /**
      Set Health RGB LEDs
      Use LED_RGB_* or
      1-100 -> red to green transition
      Only writes to driver if values have changed or force
    */
    void setHealthLED(int health, bool force = false);

    /**
    Use LED_STATUS_* for this.
    Note: LEDs cannot blink at different speeds
    Only writes to driver if values have changed or force
    */
    void setStatus(int status1,int status2, bool force = false);

    /**
      Setup LED controller
      Returns true if successfull
    */
    bool setup();

  private:
    ///Only update driver if value has changed
    int status1_old;
    ///Only update driver if value has changed
    int status2_old;
    ///Only update driver if value has changed
    int health_old;
    uint8_t reg_driver1;
    uint8_t reg_driver2;
    int green_dimm;//Percent

    void writeLEDDriver();
  };
} //namespace
#endif
