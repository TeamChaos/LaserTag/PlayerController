#ifndef FEEDBACKHANDLER_H
#define FEEDBACKHANDLER_H

#include <stdint.h>
#include "spi_handler.h"

#define SOUND_ID_GAME_START 0x04
#define SOUND_ID_RESPAWN 0x07
#define SOUND_ID_HEAL 0x09
#define SOUND_ID_REFILL 0x0A
#define SOUND_ID_HURT 0x03
#define SOUND_ID_DEATH 0x0D
#define SOUND_ID_GAMEEND 0x05
#define SOUND_ID_BOOT 0x0E
#define SOUND_ID_GAMEPOINTS 0x08

#define LONG_VIB_DURATION 1000 //ms
#define SHORT_VIB_DURATION 500 //ms

#define MOTOR_COUNT 6

#define MOTOR1_PIN 14 
#define MOTOR2_PIN 27
#define MOTOR3_PIN 26 
#define MOTOR4_PIN 25 
#define MOTOR5_PIN 33 
#define MOTOR6_PIN 32 

namespace signal_output {

  class FeedbackHandler {

  public:
    FeedbackHandler(comm_interfaces::SPIHandler *spi_);
    void playSound(uint8_t sound);
    void triggerMotor(bool activateMotor[MOTOR_COUNT], bool longVib);
    void tick();
    void init();

  private:
    comm_interfaces::SPIHandler *spi;
    int motorPins[MOTOR_COUNT] ; //GPIOs of ESP32 which motors are attached to
    bool motorActive[MOTOR_COUNT]; // whether motor is currently running
    int motorStartTime[MOTOR_COUNT]; // stores time stamps when motors are triggered
    int motorDuration; // dynamically set to desired duration of activation
    uint8_t isd_check_result1;
    uint8_t isd_check_result2;
    bool checkISD();
  };
}
#endif
