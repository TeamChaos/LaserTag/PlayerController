#include "feedback_handler.h"
#include "debug_handler.h"
#include "i2c_util.h"

using namespace signal_output;

FeedbackHandler::FeedbackHandler(comm_interfaces::SPIHandler *spi_): spi(spi_), motorPins{MOTOR1_PIN, MOTOR2_PIN, MOTOR3_PIN, MOTOR4_PIN, MOTOR5_PIN, MOTOR6_PIN},
  motorActive{false}{
  for (int i = 0; i < MOTOR_COUNT; i++) {
    pinMode(motorPins[i], OUTPUT);
    digitalWrite(motorPins[i], LOW);
  }
}

void FeedbackHandler::playSound(uint8_t sound){
    checkISD();
    if (~isd_check_result1 & 0b01000000) {
        //Not ready
        debug::DH.error(TAG_SOUND,"Failed to trigger sound. ISD not ready: "+String(isd_check_result1,BIN)+" "+String(isd_check_result2,BIN));
        return;
    } else if (isd_check_result1 & 0b10) {
        //Channel still busy
        debug::DH.info(TAG_SOUND,"Sound still playing. Canceling "+String(isd_check_result1,BIN)+" "+String(isd_check_result2,BIN));
        uint8_t data[1];
        data[0]=0x2a;
        spi->writeSound(1,data);
        delay(1);


    } else if (isd_check_result2 & 0b10000) {
        //There has been an error before
        debug::DH.warn(TAG_SOUND,"There has been an playback error before. "+String(isd_check_result1,BIN)+" "+String(isd_check_result2,BIN));
        uint8_t data[4];
        data[0] = 0x40;
        data[1] = 0x00;
        data[2] = 0x00;
        data[3] = 0x00;
        spi->writeSound(4, data);
    }
    uint8_t data[3];
    data[0] = 0xa6;
    data[1] = 0x00;
    data[2] = sound;
    spi->writeSound(3,data);
}

bool FeedbackHandler::checkISD() {
    uint8_t data[4];
    data[0] = 0x40;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x00;
    spi->writeSound(4, data);
    isd_check_result1 = data[0];
    isd_check_result2 = data[1];
    return (isd_check_result1 >> 6) == 0b01; //Status should be 0b01xxxxxx
}


void FeedbackHandler::triggerMotor(bool activateMotor[MOTOR_COUNT], bool longVib){
  motorDuration = longVib ? LONG_VIB_DURATION : SHORT_VIB_DURATION;
  for (int i = 0; i < MOTOR_COUNT; i++) {
    if (activateMotor[i]) {
      digitalWrite(motorPins[i], HIGH);
      motorStartTime[i] = millis();
      motorActive[i] = true;
    }
  }
}

void FeedbackHandler::init(){
      checkISD();
      if(isd_check_result1 & 0b10000000){
        debug::DH.warn(TAG_SOUND,"ISD powered down. Powering up");
        uint8_t data[1];
        data[0] = 0x10;
        spi->writeSound(1,data);

      }
      uint8_t data[1];
      data[0]=0x14;
      spi->writeSound(1,data);
      checkISD();
      if(isd_check_result1 != 0b01000000 && isd_check_result1 != 0b01100000){
        debug::DH.error(TAG_SOUND,"ISD in unknown state: "+String(isd_check_result1,BIN)+" "+String(isd_check_result2,BIN));
      }
}

void FeedbackHandler::tick() {
  for (int i = 0; i < MOTOR_COUNT; i++) {
    if (motorActive[i]) {
      // check termination 
      if (millis() - motorStartTime[i] > motorDuration) {
        digitalWrite(motorPins[i], LOW);
        motorActive[i] = false;
      }
    }
  }
}