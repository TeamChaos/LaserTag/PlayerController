#include "led_controller.h"
#include "i2c_util.h"
#include "debug_handler.h"


using namespace signal_output;

#define I2C_LED_ADDRESS 0x14
//Auto increments to mode 2
#define I2C_LED_MODE 0x80
#define I2C_LED_MODE1_VALUE 0x00
#define I2C_LED_MODE2_VALUE 0b100001

//Write Red,Green,Blue values consecutively after this
#define I2C_LED_HEALTH_PWM 0xA2

#define I2C_LED_GROUP_DUTY 0x0A
#define I2C_LED_GROUP_FREQ 0x0B
#define I2C_LED_GROUP_FREQ_FAST 0x0A
#define I2C_LED_GROUP_FREQ_SLOW 0x17


#define I2C_LED_DRIVER 0x0C
#define I2C_LED_HEALTH_RED 0
#define I2C_LED_HEALTH_GREEN 2
#define I2C_LED_HEALTH_BLUE 4



LedController::LedController() {
    green_dimm=25;
}

void LedController::turnOff(){
    reg_driver1=0x00;
    reg_driver2=0x00;
    writeLEDDriver();
}


void LedController::writeLEDDriver(){
  uint8_t buffer[3];
  buffer[0]=I2C_LED_DRIVER  | 0x80; //Auto-increment
  buffer[1]=reg_driver1;
  buffer[2]=reg_driver2;
  uint8_t status = util::i2c::write(I2C_LED_ADDRESS,buffer,3);
  if(status!=0){
    debug::DH.error(TAG_LED_HANDLER,"Failed to write led driver status. Result: "+String(status,HEX));
  }
}


void LedController::setHealthLED(int health, bool force){
  //Only write if value has changed or force
  if(!force&&health==health_old)return;
  health_old = health;

  reg_driver1 &= ~( 0b11 << I2C_LED_HEALTH_RED | 0b11 << I2C_LED_HEALTH_GREEN | 0b11 << I2C_LED_HEALTH_BLUE); //Turn RGB off
  uint8_t pwm_buffer[4];//First byte is reserved for control byte
  if(health==LED_RGB_VIOLET){
    //Able to respawn
    reg_driver1 |= (0b10 << I2C_LED_HEALTH_BLUE | 0b10 << I2C_LED_HEALTH_RED) ;
    pwm_buffer[1]=0x04;
    pwm_buffer[3]=0x08;
  }
  else if(health==LED_RGB_WHITE){
    reg_driver1 = ( 0b1 << I2C_LED_HEALTH_RED | 0b1 << I2C_LED_HEALTH_GREEN | 0b1 << I2C_LED_HEALTH_BLUE);
  }
  else if(health==LED_RGB_BLUE){
    reg_driver1 |= (0b10 << I2C_LED_HEALTH_BLUE ) ;
    pwm_buffer[3]=0x20;
  }
  else if(health==LED_RGB_PINK){
    //Invulnerable indicator
    reg_driver1 |= (0b10 << I2C_LED_HEALTH_BLUE | 0b10 << I2C_LED_HEALTH_RED) ;
    pwm_buffer[1]=0x3D;
    pwm_buffer[3]=0x25;
  }
  else if(health>0){
    reg_driver1 |= (0b10 << I2C_LED_HEALTH_RED | 0b10 << I2C_LED_HEALTH_GREEN);
    pwm_buffer[1]= 255 - sqrt(health)*25;//Red
    pwm_buffer[2]= (uint8_t)(health*health*health*25/10*green_dimm/100/100/100);//Green
  }
  pwm_buffer[0]=I2C_LED_HEALTH_PWM;
  uint8_t status = util::i2c::write(I2C_LED_ADDRESS,pwm_buffer,4);
  if(status!=0){
    debug::DH.error(TAG_LED_HANDLER,"Failed to write health led pwm. Result: "+String(status,HEX));
  }
  writeLEDDriver();
}

void LedController::setStatus(int status1,int status2, bool force){
  if(!force&&status1==status1_old&&status2==status2_old)return;
  status1_old=status1;
  status2_old=status2;

  uint8_t blink_freq=0;
  reg_driver1 &= ~(0b11000000); //Turn off status 1
  reg_driver2 &= ~(0b11); //Turn of status 2
  switch(status1){
    case LED_STATUS_OFF:
      break;
    case LED_STATUS_ON:
      reg_driver1 |= 0b01000000;
      break;
    case LED_STATUS_BLINK_FAST:
      reg_driver1 |= 0b11000000;
      blink_freq = I2C_LED_GROUP_FREQ_FAST;
      break;
    case LED_STATUS_BLINK_SLOW:
      reg_driver1 |= 0b11000000;
      blink_freq = I2C_LED_GROUP_FREQ_SLOW;
      break;
  }
  switch(status2){
    case LED_STATUS_OFF:
      break;
    case LED_STATUS_ON:
      reg_driver2 |= 0b01;
      break;
    case LED_STATUS_BLINK_FAST:
      reg_driver2 |= 0b11;
      blink_freq = I2C_LED_GROUP_FREQ_FAST;
      break;
    case LED_STATUS_BLINK_SLOW:
      reg_driver2 |= 0b11;
      blink_freq = I2C_LED_GROUP_FREQ_SLOW;
      break;
  }
  if(blink_freq !=0){
    uint8_t status = util::i2c::write(I2C_LED_ADDRESS,I2C_LED_GROUP_FREQ,blink_freq);
    if(status!=0){
      debug::DH.error(TAG_LED_HANDLER,"Failed to write group blink. Result: "+String(status,HEX));
    }
  }
  writeLEDDriver();

}

bool LedController::setup(){
  uint8_t buffer[3];
  buffer[0]=I2C_LED_MODE;
  buffer[1]=I2C_LED_MODE1_VALUE;
  buffer[2]=I2C_LED_MODE2_VALUE;
  uint8_t status = util::i2c::write(I2C_LED_ADDRESS,buffer,3);//Write mode bytes to set open-drain structure and enable IC

  status |= util::i2c::write(I2C_LED_ADDRESS,I2C_LED_GROUP_DUTY,0x60); //Setup group duty cycle to around 40%
  buffer[0]=0x85; //PWM for status 1 + Autoincrement to status 2
  buffer[1]=0xFF;
  buffer[2]=0xFF;
  status |= util::i2c::write(I2C_LED_ADDRESS,buffer,3); //Set status PWM to max duty cycle
  return status ==0;
}
