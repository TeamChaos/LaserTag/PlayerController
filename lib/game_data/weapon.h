#ifndef WEAPON_H
#define WEAPON_H

#include <Arduino.h>
#include "weapon_desc.h"

namespace entity {

  class Weapon {

  public:
    Weapon();

    /**
    Configure weapon per I2C
    @param id Player id. Range 0-16
    @param damage Damage. Is rounded to multiples of four. Exception 1 -> 1 and -1 -> infinite
    @param delay Fire delay in ms. Rounded to multiples of 40. Lowest reasonable value is currently 320ms. (Max 10000)
    @param fullauto If full auto or semi-auto.
    @param magsize Number of shots per magazine.
    @param initial_mags Number of initial magazines or (-1) infinite.
    @param reload_time Reload time in ms. Rounded to multiples of 80. (Max 20000)
    */
    bool configure(unsigned int id, int damage, int delay, bool fullauto, int magsize, int initial_mags, int reload_time );

    bool configure(unsigned int id, const data::WeaponDesc* desc);

    /// enable weapon
    void enable();

    /// enable weapon delayed;
    void enable(int delay);

    /// disable weapon
    void disable();

    /// reset weapon to unconfigured
    void reset();

    /// tick
    void tick();

    /// increase stock of ammunition
    void increaseAmmo(uint8_t ammo);

    /// Don't call more often than necessary
    /// returns relative fill level of current magazine (0...1)
    float magazineLevel();

    //Dont't call more often than necessary
    /// returns amount of remaining ammunition (magazines)
    int getMagazineCount();

    ///Overwrite the magazine count
    void setMagazineCount(int count);

    ///Get weapon id
    int getWeaponId();

    ///Get weapon status
    uint getStatus();

    ///Blink kill confirm LED
    void confirmKill();

  private:
    /*
    Write buffer via I2C to weapon. Log if there is an issue
    */
    void gracefulWrite(const uint8_t *buffer, size_t length);
    /*
    Write value to address/id via I2C to weapon. Log if there is an issues
    */
    void gracefulWrite(const uint8_t id, const uint8_t data);

    /**
    Read single byte from given id/address. Returns 0 if there has been an error.
    **/
    uint8_t gracefulRead(const uint8_t id);

    uint8_t clamp(int value);

    bool waitingForEnable;

    unsigned int enableTime;
  };
}

#endif
