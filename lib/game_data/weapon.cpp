#include "weapon.h"
#include "i2c_util.h"
#include "debug_handler.h"
#include "../../CommonLib/weapon_control/protocol.h"

using namespace entity;


Weapon::Weapon() {

}

uint8_t Weapon::clamp(int value){
  return std::max(0x00, std::min(0xFF,value));
}

bool Weapon::configure(unsigned int id, int damage, int delay, bool fullauto, int magsize, int initial_mags, int reload_time ){
  uint8_t buffer[10];
  buffer[0] = I2C_SETUP1; //Address
  buffer[1] = 0; //Setup1
  buffer[2] = id;
  buffer[3] = damage == -1 ? 0xFF : (damage == 1 ? 0x00 : clamp(damage /4));
  buffer[4] = clamp(delay / 40);
  buffer[5] = fullauto ? 0x00 : 0x01;
  buffer[6] = clamp(magsize);
  buffer[7] = clamp(reload_time / 40);
  buffer[8] = clamp(initial_mags);
  buffer[9] = 0; //Setup2

  gracefulWrite(buffer,10);
  uint8_t status = gracefulRead(I2C_STATUS);
  if(status==0xFF){
    return true;
  }
  else{
    debug::DH.error(TAG_WEAPON,"Failed to configure weapon");
    return false;
  }
}

bool Weapon::configure(unsigned int id, const data::WeaponDesc* desc){
  return configure(id,desc->damage,desc->delay,desc->fullauto,desc->magsize,desc->initial_mags,desc->reload_time);
}

void Weapon::enable() {
  waitingForEnable=false;
  gracefulWrite(I2C_ENABLE, 1);
}

void Weapon::enable(int delay) {
  waitingForEnable=true;
  enableTime = millis() + delay;
}

void Weapon::disable() {
    waitingForEnable=false;
    gracefulWrite(I2C_ENABLE, 0);
}

void Weapon::reset() {
  disable(); //TODO add reset command to weapon
}


void Weapon::increaseAmmo(uint8_t ammo) {
    gracefulWrite(I2C_ADD_MAG, ammo);
}

float Weapon::magazineLevel() {
    uint8_t shots = gracefulRead(I2C_AMMUNITION);
    //return shots / magazine_size;
    return 1;
}

int Weapon::getMagazineCount() {
    return gracefulRead(I2C_MAGS);
}

void Weapon::setMagazineCount(int amount){
   gracefulWrite(I2C_MAGS, amount);
}

int Weapon::getWeaponId(){
  return gracefulRead(I2C_WEAPON_ID);
}

uint Weapon::getStatus(){
  return gracefulRead(I2C_STATUS);
}

void Weapon::confirmKill(){
  gracefulWrite(I2C_KILL_CONFIRM, 0x0F);
}

void Weapon::gracefulWrite(const uint8_t *buffer, size_t length){

  byte result = util::i2c::write(WEAPON_I2C_ADDRESS,buffer,length);
  if(result!=0){
    debug::DH.error(TAG_WEAPON,"Failed to write weapon control via I2C. Result "+String(result));
  }
}

void Weapon::gracefulWrite(const uint8_t id, const uint8_t data){
  uint8_t buffer[2];
  buffer[0] = id;
  buffer[1] = data;
  gracefulWrite(buffer, 2);
}

uint8_t Weapon::gracefulRead(const uint8_t id){

  uint8_t response;
  byte result = util::i2c::read(WEAPON_I2C_ADDRESS, id,&response);
  if(result!=0){
    debug::DH.error(TAG_WEAPON,"Failed to read from weapon via I2C. Could not write address. Result "+String(result));
    return 0;
  }
  return response;
}

void Weapon::tick(){
  if(waitingForEnable){
    if(millis()>enableTime){
      waitingForEnable=false;
      enable();
    }
  }
}
