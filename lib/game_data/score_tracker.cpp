#include "score_tracker.h"
#include "debug_handler.h"


using namespace game_analysis;

ScoreTracker::ScoreTracker(int playerID_, game_management::gameSetup &setup_): playerID(playerID_), setup(setup_), assistMarker(0) {
  //setup score chart
  playerCount = 10; //Always use 10 players as used player ids don't have to be consequetively
  std::vector<std::vector<scoreData>> tmp(playerCount, std::vector<scoreData>(playerCount));
  scoreChart = tmp;
}

void ScoreTracker::updateHit(int offenderID, bool isDead) {
    debug::DH.info(TAG_SCORE,"HIT-> update Score chart");
    if(isDead) {
      debug::DH.info(TAG_SCORE,setup.players.at(0).name);
      debug::DH.info(TAG_SCORE,setup.players.at(5).name);
      debug::DH.info(TAG_SCORE,"Off: "+String(offenderID));
      debug::DH.info(TAG_SCORE,setup.players.at(offenderID).name);


      // update score chart, player list and store transmitdata
      scoreChart.at(playerID).at(offenderID).kills++;
      scoreChart.at(playerID).at(playerID).deaths++;

      setup.players.at(offenderID).kills++;
      
      updateScore(offenderID, 1, 0);

      //since death count is absolutly known, directly update gaming setups player list
      setup.players.at(playerID).deaths++;

      if(assistMarker) {
        processAssists(offenderID);
      }

      assignTransmitData(offenderID);
      
      // reset assist marker
      assistMarker = 0;

      //debug
      printPlayerStats();
    }
    else {
      if (!isEnemy(offenderID)) return;

      debug::DH.debug(TAG_SCORE,"Store potential assist: "+String(offenderID));

      //offender potentially gets an assist, recent assist gets overwritten
      if(offenderID!=playerID) assistMarker |= 1 << offenderID; //Can't assist yourself
    }
    debug::DH.info(TAG_SCORE,"Updated");


}

void ScoreTracker::processAssists(int offenderID) {
  for (int assistID = 0; assistID < playerCount; assistID++) {
    if (assistID == offenderID) continue;
    if (1 << assistID & assistMarker) {
      scoreChart.at(playerID).at(assistID).assists++;
      setup.players.at(assistID).assists++; 
      updateScore(assistID, 0, 1);
    }
  }
}

void ScoreTracker::assignTransmitData(int offenderID) {
  transmitData.victimID = playerID;
  transmitData.deaths = setup.players.at(playerID).deaths;

  transmitData.offenderID = offenderID;
  transmitData.kills = scoreChart.at(playerID).at(offenderID).kills;

  for (int id = 0; id < MAX_PLAYER_COUNT; id++) {
    transmitData.assists[id] = scoreChart.at(playerID).at(id).assists;
  }
}

void ScoreTracker::updateAbsoluteHits(int offenderID, int kills, int victimID, int *assists) {
    //update absolute values of gaming setups player list
    // since there might be messages from offenderID which weren't received in the meantime
    // it's necessary to increment the lastly stored kill count by the difference to the now transmitted kills
    int recentKillCount = scoreChart.at(victimID).at(offenderID).kills;
    int increment = kills - recentKillCount;
    setup.players.at(offenderID).kills += increment;
    updateScore(offenderID, increment, 0);

    //now update score chart as well
    scoreChart.at(victimID).at(offenderID).kills = kills;

    for(int i=0;i< MAX_PLAYER_COUNT; i++){
      if(i!=victimID){
        // update absolute data of players list
        int recentAssistCount = scoreChart.at(victimID).at(i).assists;
        increment = assists[i] - recentAssistCount;
        setup.players.at(i).assists += increment;
        updateScore(i, 0, increment);

        // update score chart
        scoreChart.at(victimID).at(i).assists = assists[i];
      }
    }

    printPlayerStats();


}

void ScoreTracker::updateAbsoluteDeaths(int victimID, int deaths) {

    debug::DH.debug(TAG_SCORE,"Updating deaths for "+String(victimID)+" Deaths: "+String(deaths));
    //update absolute data of players list in gaming setup
    setup.players.at(victimID).deaths = deaths;

    //debug
    printPlayerStats();


}

bool ScoreTracker::isEnemy(int offenderID ) {
  if (!setup.teamBased) return true;
  
  //compare team ids otherwise
  int playersTeam = setup.players.at(playerID).teamID;
  int offendersTeam = setup.players.at(offenderID).teamID;
  return playersTeam != offendersTeam;
}

ScoreMessage ScoreTracker::getTransmitData() {
  return transmitData;
}


void ScoreTracker::updateScore(int id, int killIncrement, int assistIncrement) {
    //update score of player with specified id
    int scoreIncrement = setup.killMultiplier * killIncrement + setup.assistMultiplier * assistIncrement;
    setup.players.at(id).score += scoreIncrement;
    //update corresponding team score
    if(setup.mode != game_management::TEAM_DEATH_MATCH) return;
    setup.teamScore.at(setup.players.at(id).teamID) += scoreIncrement;


}

int ScoreTracker::addGamePoints(int points) {
  setup.players.at(playerID).score += points;
  scoreChart.at(playerID).at(playerID).gamePoints+=points;
  return scoreChart.at(playerID).at(playerID).gamePoints;
}

std::vector<scoreData>*  ScoreTracker::getFinalResult() {
  return &scoreChart.at(playerID);
}


void ScoreTracker::printPlayerStats() {
  //debug
  debug::DH.info(TAG_SCORE,"Player absolute score:\n id\tscore\tkills\tassists\tdeaths");

  for (int i = 0; i < setup.playerCount; i++) {
    debug::DH.info(TAG_SCORE,String(i)+"\t"+String(setup.players.at(i).score)+"\t"+String(setup.players.at(i).kills)+"\t"+String(setup.players.at(i).assists)+"\t"+String(setup.players.at(i).deaths));
  }
}
