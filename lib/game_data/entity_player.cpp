#include <algorithm>
#include <Arduino.h>

#include "entity_player.h"
#include "weapon_desc.h"
#include "debug_handler.h"

using namespace entity;

Player::Player(int playerID, int teamID): id(playerID), team(teamID), state(ALIVE), health(MAX_HEALTH), health_changed(true), spawnTime(0), deathTime(0), deathTimeout(0) {
}

int Player::getHealth() {
  switch(state){
    case RESPAWNING:
      return -1;
    case DEAD:
      return -4;
    default:
      return health;
  }
}

void Player::hit(int damage, bool head, bool body) {
  if(head){//TODO maybe decrease damage if both body and head have been hit
    damage=damage+(damage>>1);//*1.5
  }
  health -= damage;
  health = std::max(health, MIN_HEALTH);
  debug::DH.debug(TAG_PLAYER,"Been hit. Damage: "+String(damage)+" Health: "+String(health));

  if (!health) {
    debug::DH.debug(TAG_PLAYER,"We died :(");
    if(deathTimeout>0){
      debug::DH.debug(TAG_PLAYER,"Waiting respawn timeout");
      state = DEAD_COOLDOWN;
    }
    else{
      state = DEAD;
    }
    deathTime = millis();
  }
  health_changed=true;
}

void Player::cure(int med) {
  health += med;
  health = std::min(health, MAX_HEALTH);
  debug::DH.debug(TAG_PLAYER,"Been healed. Health: "+String(health));

  health_changed=true;
}

condition Player::getCondition() {
  return state;
}

void Player::setRespawnDelay(int delay){
  deathTimeout=delay;
}

void Player::respawn(int delay) {
  health = MAX_HEALTH;
  health_changed=true;
  spawnTime = millis();

  if(delay>0){
    state = RESPAWNING;
    // remember spawn time (see handleSpawnDelay)
    spawnDelay= delay;
  }
  else{
    state = ALIVE;
  }
}

int Player::getID() {
  return id;
}

int Player::getTeam() {
  return team;
}


void Player::tick() {
  if(state==RESPAWNING) {
    //Handle spawn delay
    int diff=millis() - spawnTime;
    if(diff%1000==0){
      Serial.println((spawnDelay - diff) / 1000);
      delay(1);
    }
    if ((millis() - spawnTime) > spawnDelay) {
      //player can now get shot again
      debug::DH.info(TAG_PLAYER,"Alive again");
      state = ALIVE;
      health_changed=true;
    }
  }
  else if(state==DEAD_COOLDOWN) {
    //Handle spawn delay
    int diff=millis() - deathTime;
    if(diff%1000==0){
      Serial.println((deathTime - diff) / 1000);
      delay(1);
    }
    if ((millis() - deathTime) > deathTimeout) {
      //player can now get shot again
      debug::DH.info(TAG_PLAYER,"Can respawn");
      state = DEAD;
      health_changed=true;
    }
  }
}

bool Player::healthChanged(){
  if(health_changed){
    health_changed=false;
    return true;
  }
  return false;
}

void Player::setWeapon(const data::WeaponDesc* weapon_){
  this->weapon = weapon_;
}

const data::WeaponDesc* Player::getWeapon(){
  return this->weapon;
}
