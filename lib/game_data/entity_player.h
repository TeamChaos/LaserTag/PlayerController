#include "weapon_desc.h"

#ifndef PLAYER_H
#define PLAYER_H



//todo: define max health, min health, spawndelay via game setup
#define MAX_HEALTH 100
#define MIN_HEALTH 0


namespace entity {

  /// condition of player
  enum condition {ALIVE, DEAD, DEAD_COOLDOWN, RESPAWNING};

  class Player {

  public:
    Player(int playerID, int teamID);

    /// decreases health by amount of damage
    void hit(int damage, bool head, bool body);

    /// increases health by amount of Embedded
    void cure(int med);

    /// returns health of players (-1 for respawning)
    int getHealth();

    /// returns players condition
    condition getCondition();

    /// resurrect player and set health to maximum
    void respawn(int delay);

    /// Returns ID of player
    int getID();

    /// Return ID of team
    int getTeam();

    /// Tick the player. Handles respawn delay and death cooldown
    void tick();

    void setWeapon(const data::WeaponDesc* weapon_);

    const data::WeaponDesc* getWeapon();

    /**
      Returns true if health has changed since last call
    **/
    bool healthChanged();

    /// Configure delay before player can respawn
    void setRespawnDelay(int delay);

  private:

    /// players properties
    int id;

    int team;

    condition state;

    int health;

    bool health_changed;

    /// time stamp of last respawn
    unsigned int spawnTime;

    /// time stamp of last death
    unsigned int deathTime;

    /// invulnerability after last respawn
    int spawnDelay;

    /// delay before respawn is possible
    int deathTimeout;

    const data::WeaponDesc *weapon;

  };

}
#endif
