#ifndef SCORETRACKER_H
#define SCORETRACKER_H

#include "game_setup.h"

#define ASSIST_MULTIPLIER 5
#define HIT_MULTIPLIER 20
#define MAX_PLAYER_COUNT 10

namespace game_analysis {

  //score data
  struct scoreData {
    int kills = 0;
    int assists = 0;
    int deaths = 0;
    int gamePoints = 0;
  };

  // data that is transmitted via radio or websocket / mobile data after death of this player
  struct ScoreMessage {
    int victimID; // id of victim
    int deaths; // absolute amount of deaths of victim


    int offenderID; // id of offender
    int kills; // absolute amount of offenders kills on this player
    
    // for transmission via websocket
    int assists[MAX_PLAYER_COUNT]; // absolute amount of each player's assists on this player
  };

  class ScoreTracker {

  public:

    ScoreTracker(int playerID_, game_management::gameSetup &setup_);

    /// player was hit by player with offender id: update score chart. Offender id may be the same as the players id
    void updateHit(int offenderID, bool isDead);

    /// process assist marker
    void processAssists(int offenderID);

    /// another player was hit by player with offender id and transmitted these data
    void updateAbsoluteHits(int offenderID, int kills, int victimID, int *assists);

    /// player transmits absolute amount of own deaths
    void updateAbsoluteDeaths(int victimID, int deaths);

    /// Returns true if offenderID and players id are in different teams
    bool isEnemy(int offenderID);

    /// Returns assists kills and deaths of corresponding players to be sent via websocket or radio
    ScoreMessage getTransmitData();

    /// Calculates cumulative score of specified player and stores it in player list
    void updateScore(int id, int killIncrement, int assistIncrement);

    /// Get the players row in the score data which represents the reliable local data
    std::vector<scoreData>* getFinalResult();

    /// Add general gamemode related points. Returns the new total amount
    int addGamePoints(int points);



  private:

    //debugging purpose: prints out setup.players statistics
    void printPlayerStats();

    // assigns score message which is later sent via radio or websocket
    void assignTransmitData(int offenderID);

    //score chart
    std::vector<std::vector<scoreData>> scoreChart;

    //count of players
    int playerCount;

    // id of this player
    int playerID;

    //game setup
    game_management::gameSetup setup;

    // id of offender who lastly shot but didn't kill
    int assistMarker;

    //transmit data
    ScoreMessage transmitData;
  };
}
#endif
