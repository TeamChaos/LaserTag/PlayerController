#include "i2c_util.h"


using namespace util::i2c;


byte util::i2c::write(const uint8_t id, const uint8_t *buffer, size_t length){
  Wire.beginTransmission(id);
  Wire.write(buffer,length);
  return Wire.endTransmission();
}

byte util::i2c::write(const uint8_t id, const uint8_t address, const uint8_t value){
  uint8_t buffer[2];
  buffer[0]=address;
  buffer[1]=value;
  return write(id,buffer,2);
}

byte util::i2c::read(const uint8_t id, const uint8_t address, uint8_t *buffer){
  return read(id,address,buffer,1);
}

byte util::i2c::read(const uint8_t id,const uint8_t address, uint8_t *buffer, size_t count){
  Wire.beginTransmission(id);
  Wire.write(address);
  byte result = Wire.endTransmission();
  if(result!=0){
    return result;
  }
  byte number = Wire.requestFrom(id,count);
  if(number!=count){
    return 0xFF;
  }
  for(uint i=0;i<count;i++){
    if(Wire.available()){
      buffer[i]=Wire.read();
    }
    else{
      return 0xFF;
    }
  }
  return 0;
}

String util::i2c::byteToString(byte data){
  char temp[5];
  sprintf(temp,"0x%02X",data);
  return temp;
}

String util::i2c::byteToDecString(byte data){
  char temp[4];
  sprintf(temp,"%03u",data);
  return temp;
}
