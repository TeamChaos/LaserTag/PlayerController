#ifndef I2C_UTIL_H
#define I2C_UTIL_H

#include <Arduino.h>
#include <Wire.h>

namespace util {

  namespace i2c {

    /**
    Write
    @param id I2C address
    @param buffer data buffer
    @param length size of data buffer
    @return 0 if success
    */
    byte write(const uint8_t id, const uint8_t *buffer, size_t length);
    /**
    Write two bytes (address + value).
    @param id I2C address
    @param address internal address to write to
    @param value value to be written to that internal address
    @return 0 if success
    */
    byte write(const uint8_t id, const uint8_t address, const uint8_t value);

    /**
    Read one byte
    @param id I2C address
    @param address internal address to read from
    @param buffer Result will be written to this location
    @return 0 if success, 0xFF if not enough bytes were read, different error code if writing address failed
    */
    byte read(const uint8_t id, const uint8_t address, uint8_t *buffer);

    /**
    Read multiple bytes
    @param id I2C address
    @param address internal address to start reading from
    @param buffer location to read to
    @param count number of bytes to read
    @return 0 if success, 0xFF if not enough bytes were read, different error code if writing address failed
    */
    byte read(const uint8_t id, const uint8_t address, uint8_t *buffer, size_t count);

    ///Convert to String in hexadecimal representation
    String byteToString(byte data);
    ///Convert to String in decimal representation
    String byteToDecString(byte data);
  }
}

#endif
