#include "weapon_desc.h"

namespace data {
  namespace weapons{
    //Damage, Delay, Full-Auto, Maxsize, Initial Mags, Reload Time
    static const WeaponDesc defaultWeapon(50,500,false,15,5,2000);
    static const WeaponDesc lowDamageWeapon(20,400,false,20,5,1700);
    static const WeaponDesc instantWeapon(-1,2000,false,10,5,3000);
    static const WeaponDesc autoWeapon(30,320,true,20,5,2000);
    static const WeaponDesc testWeapon(1,320,false,50,50,500);
  }
}
