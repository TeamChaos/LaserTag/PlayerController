#ifndef GAMESETUP_H
#define GAMESETUP_H

#include <vector>
#include <string>
#include "weapon_desc.h"

typedef unsigned short ushort;

namespace game_management {

  enum gamingMode {FREE_FOR_ALL, TEAM_DEATH_MATCH, CONQUEST, DOMINATION};

  /** 
  * TIMER: Game is started through timer running on each device separately triggered by a radio signal
  * SPAWNPOINT: Game is started for each player independently if specific spawnpoint reached
  */
  enum launchTriggerType {START_TIMER, START_SPAWNPOINT};


  struct Player {
    const char* name; //name of player
    int playerID; //id of player
    int score = 0; // score of player
    int teamID; //team id of player
    int kills = 0; // amount of kills
    int assists = 0; //amount of assists
    int deaths = 0; //amount of deaths
  };

  struct gameSetup {
    //GAME RELEVANT SETUP ////////////////////
    //gaming Mode
    gamingMode mode;
    //if the gamemode is team based
    bool teamBased;
    //whether friendly fire is enabled
    bool friendlyFire;
    //number of players participating
    int playerCount;
    // characterization of players (see struct above)
    std::vector<Player> players;
    //number of teams
    int teamCount;
    //list of teams / list of team members (player ids)
    std::vector<std::vector<int>> teams;
    //Number of capture points in Capture game mode
    int conquestPointCount;
    //Whether to send additional game updates for minor events
    bool extendedUpdates;

    //SCORING RELEVANT SETUP ////////////////////
    // multiplier of kills for score calculation
    int killMultiplier;
    // multiplier of assists for score calculation
    int assistMultiplier;
    // Generic gamemode points - awarded for a gamemode specific action
    int gameModePoints;
    // cumulative score of each team
    std::vector<int> teamScore;


    //GAME START / STOP ///////////////////////
    //launch trigger type (how game is started)
    launchTriggerType launchTrigger;
    // time which launch timer counts down if launchTrigger set to TIMER
    unsigned int launchTime;

    // PLAYER RELEVANT data
    // maximum health
    int maxHealth;
    //minimum health
    int minHealth;
    //How many seconds the weapon activation is delayed (for local) or prior (for beacon) to the respawn
    int respawnOffset;
    ///allow local respawn
    bool localRespawn;
    ///How many seconds a player must wait before being able to respawn
    int respawnDelay;
    ///Default Weapon
    const data::WeaponDesc* defaultWeapon;

  };
} //namespace
#endif
