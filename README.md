# Player Controller

Code controlling the ESP8266, the central component of the player's kit



## Function
### Pins
| Pin | GPIO | Name | Description |
| ------ | ------ | ------ | ------ |
| 4 | 36 | SENSOR_VP | ISD BSYB
| 5 | 39 | SENSOR_VN | ISD INTB
| 6 | 34 | IO34 | BTN3
| 7 | 35 | IO35 | RF_MODE_SWITCH
| 8 | 32 | IO32 | Motor 6 
| 9 | 33 | IO33 | Motor 5
| 10 | 35 | IO25 | Motor 4
| 11 | 26 | IO26 | Motor 3
| 12 | 27 | IO27 | Motor 2
| 13 | 14 | IO14 | Motor 1
| 14 | 12 | IO12 | BTN2
| 16 | 13 | IO13 | Detector Available
| 23 | 15 | IO15 | Supply Monitor
| 24 | 2 | IO2 | BTN1
| 26 | 4 | IO4 | RF_IRQ
| 27 | 16 | IO16 | RF_SDN
| 28 | 17 | IO17 | SPI_CS_ISD
| 29 | 5 | IO5 | SPI_CS_RF
| 30 | 18 | IO18 | SPI_CLK
| 31 | 19 | IO19 | SPI_MISO
| 33 | 21 | IO21 | I2C SDA
| 36 | 22 | IO22 | I2C SCL
| 37 | 23 | IO23 | SPI_MOSI


## Development Setup
Use 

```
git clone --recursive https://gitlab.com/TeamChaos/Lasertag/PlayerController.git
```

to clone this repo and the submodule.

Copy `include/secrets_default.h` to `include/secrets.h` and, if necessary, replace the dummy values with the same secrets used on the server.  

We are using Platformio (with Atom) as IDE.
