//Copy/Rename as config_secret.h and enter the configuration and secrets below
#ifndef CONFIG_H
#define CONFIG_H
//Basic encoded user and password
//SECRET for the server ESP API
#define ESP_API_AUTH "basic_secret"
#define CONFIG_HOST_DOMAIN "lasertag.maxgb.de"
#define CONFIG_HOST_PORT 443
//Uncomment if the host/port is not SSL
//#define CONFIG_HOST_INSECURE true

//Websocket url
#define CONFIG_WS_LOCATION "wss://lasertag.maxgb.de:443/ws_game"
//SSID and password for the basestation wifi
#define CONFIG_BASESTATION_WIFI_SSID "LazerMaster"
#define CONFIG_BASESTATION_WIFI_PSK "lazermaster"
//If true the basestation wifi is used instead of mobile hotspot even if mobile data is disabled
#define CONFIG_FORCE_BASESTATION_WIFI false


#endif
