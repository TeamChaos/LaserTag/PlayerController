#include <Arduino.h>
#include <Wire.h>
#include <WiFi.h>

#include "game_setup_manager.h"
#include "game_loop_manager.h"
#include "game_eval_manager.h"
#include "test_mode_manager.h"

#include "game_setup.h"

#include "wifi_handler.h"
#include "button_handler.h"
#include "radio_handler.h"
#include "ir_handler.h"
#include "feedback_handler.h"
#include "debug_handler.h"
#include "spi_handler.h"

#include "config_secret.h"


//######## PIN LAYOUT #########################

//i2c
#define SCL_PIN 22             //Pin 36
#define SDA_PIN 21            //Pin 33
//spi select pins
#define RADIO_CS_PIN 5
#define SOUND_CS_PIN 17

//infra red (receiving state)
#define IR_RECEIVE_PIN 13    

//Buttons (device control, weapon reload)
#define CONTROL_BTN_PIN 2   
#define CONTROL_BTN2_PIN 12
#define CONTROL_BTN3_PIN 34

#define SUPPLY_MONITOR_PIN 15 

#define RF_MODE_SWITCH_PIN 35

//##############################################

//Last 3 bytes in reverse order (byte itself isn't reversed)
#define MACID0 0x480D97 
#define MACID1 0x0C0D97
#define MACID2 0x500E97
#define MACID3 0x380D97
#define MACID4 0x4C0D97
#define MACID5 0xD40D97
//Don't forget to add this to the switch case

using namespace comm_interfaces;

//definition of game states
enum gameState {SETUP, RUNNING, EVALUATION};
gameState currentGameState;

//TestBootMode
bool testBootMode = false;
game_management::TestModeManager *tmm;

//manager that handle the current game state
game_management::GameSetupManager *gsm;
game_management::GameLoopManager *glm;
game_management::GameEvalManager *gem;

//communication interfaces which are used by game state managers
GameComInterface *gci;
WifiHandler *wh;
IRHandler *ih;
RadioHandler *rh;
ButtonHandler *bh;

//weapon
entity::Weapon *weapon;

//human device interfaces (speaker and leds)
signal_output::FeedbackHandler *fh;
signal_output::LedController *lh;


//###########PARAMETERS#############



// smartphone wifi hotspot when mobile data is used
char mobileAP[10];
char mobilePW[11]; 

void execSetupState() {
  gsm->loop();
  if (gsm->finished()) {//Start game

    //retrieve game setup
    game_management::gameSetup setup = gsm->getGameSetup();
    //game is now launched, prepare gameloopmanager
    glm->init(setup);
    //update game state machine
    currentGameState = RUNNING;


  }
}

void execRunningState() {
  glm->loop();
  if (glm->finished()) {
    currentGameState = EVALUATION;
    gem->submitFinalResult(glm->getFinalResult());
    gem->submitHitRecords(glm->getHitRecords());
    debug::DH.info(TAG_GAMELOOP, "Game Over");
  }
}

void execEvalState() {
  gem->loop();
  if (gem->finished()) currentGameState = SETUP;
}

bool checkPeripherals(){
  if(ih->checkStatus()==0xFF){
    return true;
    debug::DH.error(TAG_GLOBAL, "Detector not ok");
    return false;
  }
  return true;
}

void ICACHE_RAM_ATTR handleSupplyLoss(){
  //Internals should also be placed in IRAM
  Serial.println("Supply Loss");

}

 int lookupDeviceId(){
  byte mac[6];
  WiFi.macAddress(mac);
  switch( (( mac[5]<<16) | (mac[4] << 8) | mac[3] ) ){
    case MACID0:
      return 0;
    case MACID1:
      return 1;
    case MACID2:
      return 2;
    case MACID3:
      return 3;
    case MACID4:
      return 4;
    case MACID5:
      return 5;
    default:
      return -1;
  }
}

void setup() {
    pinMode(SUPPLY_MONITOR_PIN,INPUT);

    attachInterrupt(digitalPinToInterrupt(SUPPLY_MONITOR_PIN), handleSupplyLoss, FALLING);
    //Setup serial connection
    Serial.begin(115200);
    debug::DH.init(true,true,LEVEL_DEBUG,0xFFFF);

    pinMode(SUPPLY_MONITOR_PIN,INPUT);
    attachInterrupt(digitalPinToInterrupt(SUPPLY_MONITOR_PIN), handleSupplyLoss, FALLING);


    int macid = lookupDeviceId();
    if(macid==-1){
      macid = 9;
      debug::DH.warn(TAG_GLOBAL,"Failed to lookup mac address id: "+WiFi.macAddress());
    }
    else{
      debug::DH.info(TAG_GLOBAL,"Device ID: "+String(macid));
    }

    // Wether to use 433Mhz radio or mobile 
    pinMode(RF_MODE_SWITCH_PIN, INPUT);
    bool useMobileData = digitalRead(RF_MODE_SWITCH_PIN) == HIGH;
    
    // master wifi network configuration
    strcpy(mobileAP, "MobileAPx");
    mobileAP[8] = char(macid + 48);

    strcpy(mobilePW, "topsecretx");
    mobilePW[9] = char(macid + 48);

    const char* masterSSID = (useMobileData && ! CONFIG_FORCE_BASESTATION_WIFI) ? &mobileAP[0] : CONFIG_BASESTATION_WIFI_SSID; 
    const char* masterPW = (useMobileData && ! CONFIG_FORCE_BASESTATION_WIFI) ? &mobilePW[0] : CONFIG_BASESTATION_WIFI_PSK;

    Wire.setClock(10000);
    Wire.begin();

    //instantiate communication interfaces
    wh = new WifiHandler(masterSSID, masterPW,  macid, useMobileData);
    ih = new IRHandler(IR_RECEIVE_PIN);
    SPIHandler *spi = new SPIHandler(RADIO_CS_PIN, SOUND_CS_PIN);
    rh = new RadioHandler(spi, macid);
    bh = new ButtonHandler(CONTROL_BTN_PIN, CONTROL_BTN2_PIN, CONTROL_BTN3_PIN);
    gci = useMobileData ? (GameComInterface*) wh : (GameComInterface*) rh; 

    //instantiate signalization interfaces
    fh = new signal_output::FeedbackHandler(spi);
    lh = new signal_output::LedController();

    delay(3000);//Wait for peripherals to boot up

    //instantiate weapon handler
    weapon = new entity::Weapon();

    //instantiate game state manager
    gsm = new game_management::GameSetupManager(wh, gci, ih, lh, macid, useMobileData);
    glm = new game_management::GameLoopManager(wh, gci, ih, bh, lh, fh, weapon, macid);
    gem = new game_management::GameEvalManager(wh,lh,macid);

    fh->init();

    //Check peripherals
    while(!checkPeripherals()){
      delay(2000);
    }

    lh->setup();
    lh->setHealthLED(LED_RGB_BLUE,true);
    lh->setStatus(LED_STATUS_OFF,LED_STATUS_BLINK_SLOW,true);
    fh->playSound(SOUND_ID_BOOT);
    ih->startDetecting();
    weapon->reset();

    if(!digitalRead(CONTROL_BTN2_PIN) && !digitalRead(CONTROL_BTN_PIN)){
      debug::DH.warn(TAG_GLOBAL, "Starting test mode");
      testBootMode = true;
      tmm = new game_management::TestModeManager(wh,ih,bh,lh,fh,weapon);
      tmm->setup();
    }
    else if(!digitalRead(CONTROL_BTN2_PIN)){
      //----- TEST --- test, skipping setup state
      debug::DH.info(TAG_GLOBAL,"Starting game test mode");
      game_management::gameSetup setup;
      wh->connect(); // todo: retry on connection failure
      wh->requestTestSetup(setup);
      glm->init(setup);
      wh->setupWebSocketServer();
      currentGameState = RUNNING;
    } else{
      currentGameState = SETUP;

    }

    //init game state machine with SETUP state
    //currentGameState = SETUP;
    debug::DH.info(TAG_GLOBAL,"Setup complete");

}


void loop() {
    //execute game state machine
    if(!testBootMode && (bh->isReset() || wh->isResetRequested())){
      debug::DH.warn(TAG_GLOBAL,"Resetting");
      currentGameState = SETUP;
      glm->reset();
      gsm->reset();
      gem->reset();
      gci->reset();
      lh->setHealthLED(LED_RGB_BLUE,true);
      lh->setStatus(LED_STATUS_OFF,LED_STATUS_BLINK_SLOW,true);
      delay(100);
    }
    wh->tick();
    rh->tick();
    bh->tick();
    fh->tick();
    if(testBootMode){
      tmm->loop(); //If booted in test mode only execute test manager and ignore game managers
      return;
    }
    switch (currentGameState) {
      case SETUP:
        execSetupState();
        break;
      case RUNNING:
        execRunningState();
        break;
      case EVALUATION:
        execEvalState();
        break;
      default:
        break;
    }
}


